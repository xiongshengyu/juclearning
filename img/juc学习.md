---

---

# juc

## 进程，线程

1. 进程
   进程是程序的一次执行过程，是系统运行程序的基本单位，因此进程是动态的。系统运行一个程序即是一个进程从创建，运行到消亡的过程。
2. 线程
   一个进程可以有多个线程，多个线程共享进程的**堆**，**方法区**，每个线程有自己独立的**栈，PC计数器，本地方法栈**，各个线程之间作切换工作时，负担要比进程小得多，线程被叫做轻量级进程

## sleep和wait对比

* **sleep**：sleep不释放锁

* **wait：**wait会释放锁

* `wait()` 通常被用于线程间交互/通信，`sleep()`通常被用于暂停执行。

  `wait()` 方法被调用后，线程不会自动苏醒，需要别的线程调用同一个对象上的 `notify()`或者 `notifyAll()` 方法。`sleep()`方法执行完成后，线程会自动苏醒，或者也可以使用 `wait(long timeout)` 超时后线程会自动苏醒。

  `sleep()` 是 `Thread` 类的静态本地方法，`wait()` 则是 `Object` 类的本地方法。

  ***

## 线程通信判断条件用while为什么不用if

> 为了防止虚假唤醒，虚假唤醒通俗来说就是当一个条件满足时可以唤醒多个线程，但是有些被唤醒的线程是不符合 if/while 的执行条件的。
>
> **就是用 if 判断的话，唤醒后线程会从 wait 之后的代码开始运行，但是不会重新判断 if 条件，直接继续运行 if 代码块之后的代码，而如果使用 while 的话，也会从wait 之后的代码运行，但是唤醒后会重新判断循环条件，如果不成立再执行 while 代码块之后的代码块，成立的话继续 wait**

## static 和synchronized

```java
class Phone{
    public static synchronized void email(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("------email");
    }
    public synchronized void phone(){
        System.out.println("------phone");
    }
    public void hello(){
        System.out.println("hello");
    }
}
public class StaticSynchronized {
    public static void main(String[] args) {
        Phone phone = new Phone();
        new Thread(()->{
            phone.email();
        },"AA").start();
        new Thread(()->{
            phone.hello();
        },"BB").start();
    }
}
```



1. **static synchronized连用：**
   * **表示锁的对象是当前类的Class对象**，因此当多个线程访问static synchronized 方法时，只会有一个线程访问到资源，但是不影响其他线程访问**非 static synchronized**方法
2. **synchronized单独用：**
   * **表示锁的对象是当前对象资源this**，如果是同步代码块，那么锁的对象为Synchronized（obj）括号中的对象，当多个线程访问synchronized方法同样只能有一个线程访问到资源，但是，其他线程仍然可以访问static synchronized**方法

> **总结：**
>
> 1. 静态同步方法和非静态同步方法不会有竞争，因为他们锁的对象不同
> 2. 无论是静态同步方法，还是非静态同步方法，在线程访问之前都要获取锁对象，执行完，异常，必须释放锁

### synchronized底层原理

#### 同步代码块

```java
public class TestSync {
   public void test(){
       synchronized(this){
           System.out.println("hello");
       }
   }
}
```

![img](juc%E5%AD%A6%E4%B9%A0.assets/1645428692227-c460e796-19dd-48fe-a4c9-e89a71684d8a-16766523554353.png)

`synchronized`同步语句底层使用的是**monitorenter**和**monitorexit**

* **monitorenter**：指明代码块开始的位置

* **monitorexit**：指明代码块结束的位置

  > 当执行到monitorenter指令时，线程会视图获取锁，也就是获取对象监视器monitor的持有权，java虚拟机hotspot中，`Monitor`是基于c++实现的，有ObjectMonitor实现，每个对象中都内置了一个`ObjectMonitor`对象,另外，wait/notify等方法也依赖与monitor对象，这就是为什么只有在同步的块中或者方法中才能调用wait/notify等方法，否则会抛出java.lang.illegaLmonitorStateException异常

* 在执行monitorenter时，会尝试获取对象的锁，如果锁的计数器为 0 则表示锁可以被获取，获取后将锁计数器设为 1 也就是加 1。

* 在执行 monitorexit 指令后，将锁计数器设为 0，表明锁被释放。如果获取对象锁失败，那当前线程就要阻塞等待，直到锁被另外一个线程释放为止。

#### 修饰方法

```java
public synchronized void m2(){
        System.out.println("hello synchronized");
    }
```

![img](juc%E5%AD%A6%E4%B9%A0.assets/1645428692170-9fa06ca0-6bc4-49e3-9f04-2434718e51c9.png)

* `synchronized `修饰的方法并没有 `monitorenter `指令和 `monitorexit `指令，取得代之的确实是 `ACC_SYNCHRONIZED` 标识，该标识指明了该方法是一个同步方法。JVM 通过该 `ACC_SYNCHRONIZED `访问标志来辨别一个方法是否声明为同步方法，从而执行相应的同步调用。

#### 原理总结

1. synchronized同步代码块使用的是monitorenter和monitorexit指令，其中monitorenter指令指向同步代码块的开始位置，monitorexit指令指明同步代码块的结束位置
2. synchronized修饰的方法并没有monitorenter和monitorexit指令，取而代之的是**ACC_SYNCHRONIZED**表示，该表示指明了该方法是一个同步方法。
3. **不过二者的本质都是对对象监视器monitor的获取**



## :poodle: 线程池

**概念：**线程池就是管理一系列线程的资源池。当有任务要处理时，直接从线程池中获取线程来处理，处理完之后线程并不会立即被销毁，而是等待下一个任务。

### 为什么要用线程池

**降低资源消耗**。通过重复利用已创建的线程降低线程创建和销毁造成的消耗。

**提高响应速度**。当任务到达时，任务可以不需要等到线程创建就能立即执行。

**提高线程的可管理性**。线程是稀缺资源，如果无限制的创建，不仅会消耗系统资源，还会降低系统的稳定性，使用线程池可以进行统一的分配，调优和监控。

### 如何创建

1. **通过`ThreadPoolExecutor`构造函数来创建（推荐）**
2. **通过 `Executor` 框架的工具类 `Executors` 来创建。**

我们可以创建多种类型的 `ThreadPoolExecutor`：

- **`FixedThreadPool`** ： 该方法返回一个固定线程数量的线程池。该线程池中的线程数量始终不变。当有一个新的任务提交时，线程池中若有空闲线程，则立即执行。若没有，则新的任务会被暂存在一个任务队列中，待有线程空闲时，便处理在任务队列中的任务。
- **`SingleThreadExecutor`：** 该方法返回一个只有一个线程的线程池。若多余一个任务被提交到该线程池，任务会被保存在一个任务队列中，待线程空闲，按先入先出的顺序执行队列中的任务。
- **`CachedThreadPool`：** 该方法返回一个可根据实际情况调整线程数量的线程池。线程池的线程数量不确定，但若有空闲线程可以复用，则会优先使用可复用的线程。若所有线程均在工作，又有新的任务提交，则会创建新的线程处理任务。所有线程在当前任务执行完毕后，将返回线程池进行复用。
- **`ScheduledThreadPool`** ：该返回一个用来在给定的延迟后运行任务或者定期执行任务的线程池。

### 为什么不推荐使用内置线程池

因为创建的无界队列可能会导致OOM

### 线程池常见的参数

```java
    /**
     * 用给定的初始参数创建一个新的ThreadPoolExecutor。
     */
    public ThreadPoolExecutor(int corePoolSize,//线程池的核心线程数量
                              int maximumPoolSize,//线程池的最大线程数
                              long keepAliveTime,//当线程数大于核心线程数时，多余的空闲线程存活的最长时间
                              TimeUnit unit,//时间单位
                              BlockingQueue<Runnable> workQueue,//任务队列，用来储存等待执行任务的队列
                              ThreadFactory threadFactory,//线程工厂，用来创建线程，一般默认即可
                              RejectedExecutionHandler handler//拒绝策略，当提交的任务过多而不能及时处理时，我们可以定制策略来处理任务
                               ) {
        if (corePoolSize < 0 ||
            maximumPoolSize <= 0 ||
            maximumPoolSize < corePoolSize ||
            keepAliveTime < 0)
            throw new IllegalArgumentException();
        if (workQueue == null || threadFactory == null || handler == null)
            throw new NullPointerException();
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.workQueue = workQueue;
        this.keepAliveTime = unit.toNanos(keepAliveTime);
        this.threadFactory = threadFactory;
        this.handler = handler;
    }

```

**`ThreadPoolExecutor` 3 个最重要的参数：**

- **`corePoolSize` :** 任务队列未达到队列容量时，最大可以同时运行的线程数量。
- **`maximumPoolSize` :** 任务队列中存放的任务达到队列容量的时候，当前可以同时运行的线程数量变为最大线程数。
- **`workQueue`:** 新任务来的时候会先判断当前运行的线程数量是否达到核心线程数，如果达到的话，新任务就会被存放在队列中。

`ThreadPoolExecutor`其他常见参数 :

- **`keepAliveTime`**:线程池中的线程数量大于 `corePoolSize` 的时候，如果这时没有新的任务提交，核心线程外的线程不会立即销毁，而是会等待，直到等待的时间超过了 `keepAliveTime`才会被回收销毁；
- **`unit`** : `keepAliveTime` 参数的时间单位。
- **`threadFactory`** :executor 创建新线程的时候会用到。
- **`handler`** :拒绝策略 

### 线程池的饱和策略

如果当前同时运行的线程数量达到最大线程数量并且队列也已经被放满了任务时，`ThreadPoolTaskExecutor` 定义一些策略:

> 1. **`ThreadPoolExecutor.AbortPolicy`：** 抛出 `RejectedExecutionException`来拒绝新任务的处理。
>
> 2. **`ThreadPoolExecutor.CallerRunsPolicy`：** 调用执行自己的线程运行任务，也就是直接在调用`execute`方法的线程中运行(`run`)被拒绝的任务，如果执行程序已关闭，则会丢弃该任务。因此这种策略会降低对于新任务提交速度，影响程序的整体性能。如果您的应用程序可以承受此延迟并且你要求任何一个任务请求都要被执行的话，你可以选择这个策略。
>
> 3. **`ThreadPoolExecutor.DiscardPolicy`：** 不处理新任务，直接丢弃掉。
>
> 4. **`ThreadPoolExecutor.DiscardOldestPolicy`：** 此策略将丢弃最早的未处理的任务请求



### 程池常用的阻塞队列

新任务来的时候会先判断当前运行的线程数量是否达到核心线程数，如果达到的话，新任务就会被存放在队列中。

> 不同的线程池会选用不同的阻塞队列，我们可以结合内置线程池来分析。
>
> - 容量为 `Integer.MAX_VALUE` 的 `LinkedBlockingQueue`（无界队列）：`FixedThreadPool` 和 `SingleThreadExector` 。由于队列永远不会被放满，因此`FixedThreadPool`最多只能创建核心线程数的线程。
> - `SynchronousQueue`（同步队列） ：`CachedThreadPool` 。`SynchronousQueue` 没有容量，不存储元素，目的是保证对于提交的任务，如果有空闲线程，则使用空闲线程来处理；否则新建一个线程来处理任务。也就是说，`CachedThreadPool` 的最大线程数是 `Integer.MAX_VALUE` ，可以理解为线程数是可以无限扩展的，可能会创建大量线程，从而导致 OOM。
> - `DelayedWorkQueue`（延迟阻塞队列）：`ScheduledThreadPool` 和 `SingleThreadScheduledExecutor` 。`DelayedWorkQueue` 的内部元素并不是按照放入的时间排序，而是会按照延迟的时间长短对任务进行排序，内部采用的是“堆”的数据结构，可以保证每次出队的任务都是当前队列中执行时间最靠前的。`DelayedWorkQueue` 添加元素满了之后会自动扩容原来容量的 1/2，即永远不会阻塞，最大扩容可达 `Integer.MAX_VALUE`，所以最多只能创建核心线程数的线程。

### :imp: 线程池处理任务的流程

![图解线程池实现原理](https://gitee.com/xiongshengyu/juclearning/raw/master/img/%E5%9B%BE%E8%A7%A3%E7%BA%BF%E7%A8%8B%E6%B1%A0%E5%AE%9E%E7%8E%B0%E5%8E%9F%E7%90%86.png)

> 1. 如果当前运行的线程数小于核心线程数，那么就会新建一个线程来执行任务。
> 2. 如果当前运行的线程数等于或大于核心线程数，但是小于最大线程数，那么就把该任务放入到任务队列里等待执行。
> 3. 如果向任务队列投放任务失败（任务队列已经满了），但是当前运行的线程数是小于最大线程数的，就新建一个线程来执行任务。
> 4. 如果当前运行的线程数已经等同于最大线程数了，新建线程将会使当前运行的线程超出最大线程数，那么当前任务会被拒绝，饱和策略会调用`RejectedExecutionHandler.rejectedExecution()`方法

### 如何设定线程池的大小？

**CPU 密集型任务(N+1)：** 这种任务消耗的主要是 CPU 资源，可以将线程数设置为 N（CPU 核心数）+1。比 CPU 核心数多出来的一个线程是为了防止线程偶发的缺页中断，或者其它原因导致的任务暂停而带来的影响。一旦任务暂停，CPU 就会处于空闲状态，而在这种情况下多出来的一个线程就可以充分利用 CPU 的空闲时间。

**I/O 密集型任务(2N)：** 这种任务应用起来，系统会用大部分的时间来处理 I/O 交互，而线程在处理 I/O 的时间段内不会占用 CPU 来处理，这时就可以将 CPU 交出给其它线程使用。因此在 I/O 密集型任务的应用中，我们可以多配置一些线程，具体的计算方法是 2N。

**如何判断是 CPU 密集任务还是 IO 密集任务？**

CPU 密集型简单理解就是利用 CPU 计算能力的任务比如你在内存中对大量数据进行排序。但凡涉及到网络读取，文件读取这类都是 IO 密集型，这类任务的特点是 CPU 计算耗费时间相比于等待 IO 操作完成的时间来说很少，大部分时间都花在了等待 IO 操作完成上







## 锁

### 悲观锁和乐观锁



### 公平锁与非公平锁

1. **公平锁**：每个线程获取锁的顺序是按照线程访问锁的先后顺序获取的，最前面的线程总是最先获取到锁。
   * **好处**：避免了线程饥饿，能够保证线程获取锁的顺序是按照请求锁的顺序。
   * 坏处：需要进行线程切换和调度，因此可能会增加系统的开销和延迟。

2. **非公平锁**：每个线程获取锁的顺序是随机的，并不会遵循先来先得的规则，所有线程会竞争获取锁。
   * 好处：不需要进行线程切换调度，减少系统的开销
   * 坏处：可能会导致某些线程长期获取不到锁，导致线程饥饿问题出现

**例子说明**

```java
class Ticket{
    public int ticket=30;
   public final ReentrantLock lock = new ReentrantLock();
    public void saleTicket(){
        lock.lock();
        try {
            if (ticket>0){
                System.out.println(Thread.currentThread().getName()+"抢到第"+ticket--+"剩余:"+ticket);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

    }
}
public class TicketTest {
    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        new Thread(()->{
            for (int i = 0; i < 40; i++) ticket.saleTicket();
        },"A").start();
        new Thread(()->{
            for (int i = 0; i < 40; i++) ticket.saleTicket();
        },"B").start();
        new Thread(()->{
            for (int i = 0; i < 40; i++) ticket.saleTicket();
        },"C").start();
    }
}

```



![image-20230218014748130](juc学习.assets/image-20230218014748130.png)

> 以上代码演示的**非公平锁**，结果就是线程B没有抢到锁，导致线程饥饿
>
> **public final ReentrantLock lock = new ReentrantLock();**该锁为非公平锁

> 当我们将非公平锁修改为公平锁，**public final ReentrantLock lock = new ReentrantLock(true)**，执行结果如下，可以看到三个线程都有机会抢到锁。不会有线程饥饿问题出现

![image-20230218015054595](juc学习.assets/image-20230218015054595.png)

### 可重入锁

synchronized默认就是可重入锁

* 线程可以多次获取同一把锁而不会被阻塞。通俗地说，一个线程在持有某个锁的情况下，如果再次请求该锁，请求会成功，而不会被阻塞。

* 可重入锁的实现通常使用**计数器**来**记录锁的持有次数**，每次加锁都会将计数器加1，每次解锁都会将计数器减1，**只有当计数器归零时，其他线程才能获取该锁。**

```java
public class WhatReentrant {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (this) {
                    System.out.println("第1次获取锁，这个锁是：" + this);
                    int index = 1;
                    while (true) {
                        synchronized (this) {
                            System.out.println("第" + (++index) + "次获取锁，这个锁是：" + this);
                        }
                        if (index == 10) {
                            break;
                        }
                    }
                }
            }
        }).start();
    }
}
```

### ObjectMonitor和Synchronized总结

![image-20230218020724651](juc%E5%AD%A6%E4%B9%A0.assets/image-20230218020724651.png)

![image-20230218020911179](juc%E5%AD%A6%E4%B9%A0.assets/image-20230218020911179.png)

## 线程中断协商机制

### 如何使用中断标识停止线程

1. #### 通过一个volatile变量实现

```java
public class interruptDemo {
    static volatile boolean isStop = false;

    public static void main(String[] args) {
        new Thread(()->{
            while(true){
                if(isStop){//如果这个标志位被其他线程改为true了
                    System.out.println(Thread.currentThread().getName()+"\t isStop被修改为true，程序终止");
                    break;
                }
                System.out.println("t1 ------hello volatile");//----------------------如果没停止，那就一直打印
            }
        },"t1").start();

        try {TimeUnit.MILLISECONDS.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}

        new Thread(()->{
            isStop = true;
        },"t2").start();
    }
}
//--
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1   isStop被修改为true，程序终止

```

2. #### 通过AtomicBoolean（原子布尔型）

```java
public class interruptDemo {

    static AtomicBoolean atomicBoolean = new AtomicBoolean(false);

    public static void main(String[] args) {
        m1_volatile();
    }

    public static void m1_volatile() {
        new Thread(()->{
            while(true){
                if(atomicBoolean.get()){//如果这个标志位被其他线程改为true了
                    System.out.println(Thread.currentThread().getName()+"\t isStop被修改为true，程序终止");
                    break;
                }
                System.out.println("t1 ------hello volatile");//----------------------如果没停止，那就一直打印
            }
        },"t1").start();

        try {TimeUnit.MILLISECONDS.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}

        new Thread(()->{
            atomicBoolean.set(true);
        },"t2").start();
    }
}
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1 ------hello volatile
//t1   isStop被修改为true，程序终止

```

3. #### 通过Thread类自带的中断api方法实现

```java
    //默认的中断标志位是false，然后被改为了true
    public static void main(String[] args) {
        m1_volatile();
    }

    public static void m1_volatile() {
        Thread t1 = new Thread(() -> {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {//一旦发现中断标志位被修改
                    System.out.println(Thread.currentThread().getName() + "\t isInterrupted()被修改为true，程序终止");
                    break;
                }
                System.out.println("t1 ------hello interrupt ");//----------------------如果没停止，那就一直打印
            }
        }, "t1");
        t1.start();

        try {TimeUnit.MILLISECONDS.sleep(20);} catch (InterruptedException e) {e.printStackTrace();}

        new Thread(()->{
            t1.interrupt();//把t1中断
        },"t2").start();
    }
}

//t1 ------hello interrupt 
//t1 ------hello interrupt 
//t1 ------hello interrupt 
//t1 ------hello interrupt 
//t1 ------hello interrupt 
//t1 ------hello interrupt 
//t1 ------hello interrupt 
//t1 ------hello interrupt 
//t1 ------hello interrupt 
//t1   isInterrupted()被修改为true，程序终止

```

#### 中断相关的api

![在这里插入图片描述](juc%E5%AD%A6%E4%B9%A0.assets/38f14981ccb54f58b7dfb21f3e9d4747.png)

| public void interrupt()             | **实例方法，实例方法interrupt()仅仅是设置线程的中断状态为true，发起一个协商而不会立刻停止线程** |
| ----------------------------------- | ------------------------------------------------------------ |
| public static boolean interrupted() | 静态方法，`Thread.interrupted();`判断线程是否被中断，并清除当前中断状态这个方法做了**两件事**：1 返回当前线程的中断状态2 将当前线程的中断状态设为false（这个方法有点不好理解，因为连续调用两次的结果可能不一样。） |
| public boolean isInterrupted()      | 实例方法，判断当前线程是否被中断（通过检查中断标志位）       |

1. **interrupt()分析**

> * 具体来说，当对一个线程，调用 interrupt() 时：
>
>   1. 如果线程处于正常活动状态，那么会将该线程的中断标志设置为 true，仅此而已。被设置中断标志的线程将继续正常运行，不受影响。所以， interrupt() 并不能真正的中断线程，需要被调用的线程自己进行配合才行。
>
>   2.  如果线程处于被阻塞状态（例如处于sleep, wait, join 等状态），在别的线程中调用当前线程对象的interrupt方法，那么线程将立即退出被阻塞状态（中断状态将被清除），并抛出一个InterruptedException异常。
>   3. 中断不活动的线程不会产生任何影响

2.   **静态方法Thread.interrupted()**

> 1. 判断线程是否被中断，并清除当前中断状态
> 2. 这个方法做了**两件事**：1 、返回当前线程的中断状态2 、将当前线程的中断状态设为false

```java
public class InterruptDemo04 {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName()+"\t"+Thread.interrupted());
        System.out.println(Thread.currentThread().getName()+"\t"+Thread.interrupted());
        System.out.println("-----1");
        Thread.currentThread().interrupt();//中断标志位设置为true
        System.out.println("-----2");
        System.out.println(Thread.currentThread().getName()+"\t"+Thread.interrupted());//返回中断状态位true，之后清除中断状态设置为false
        System.out.println(Thread.currentThread().getName()+"\t"+Thread.interrupted());
    }
}
//main  false
//main  false
//-----1
//-----2
//main  true
//main  false

```

3. **实例方法isInterrupted()**

> 1. 该方法会返回一个boolean值，代表调用线程的中断状态

4. **interrupted()`对比`isInterrupted()**

```java
public static boolean interrupted() {
        return currentThread().isInterrupted(true);
    }
    
private native boolean isInterrupted(boolean ClearInterrupted);

```

```java
public boolean isInterrupted() {
        return isInterrupted(false);
    }

private native boolean isInterrupted(boolean ClearInterrupted);
```

* 他们在底层都调用了native方法isInterrupted。

* 只不过传入参数ClearInterrupted一个传参传了true，一个传了false。

* 静态方法interrupted() 中true表示清空当前中断状态。

* 实例方法isInterrupted 则不会。

## LockSuport

用于创建锁和其他同步类的基本线程阻塞原语。(阻塞，唤醒)

* **核心**就是`park()`和`unpark()`方法

- `park()`方法是**阻塞线程**
- `unpark()`方法是**解除阻塞线程**

![请添加图片描述](juc%E5%AD%A6%E4%B9%A0.assets/1ca493a6ed07446a90dafaef90d25d23.png)

### 线程等待唤醒机制

**3种让线程等待和唤醒的方法**

1. 使用Object类的`wait`方法让线程阻塞，使用`notify`方法让线程唤醒
2. 使用JUC包中的`Condition`的`await`方法让线程阻塞，使用`signal`方法让线程唤醒
3. `LockSupport`类可以阻塞当前线程以及唤醒指定被阻塞的线程

**Object类中的wait和notify方法实现线程等待和唤醒**

```java
public class LockSupportDemo
{
    public static void main(String[] args)
    {
        Object objectLock = new Object();

        new Thread(() -> {
            synchronized (objectLock) {
                System.out.println(Thread.currentThread().getName()+"\t ---- come in");
                try {
                    objectLock.wait();//----------------------这里先让他等待
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(Thread.currentThread().getName()+"\t"+"---被唤醒了");
        },"t1").start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(3L); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            synchronized (objectLock) {
                objectLock.notify();//-------------------------再唤醒它
                System.out.println(Thread.currentThread().getName()+"\t ---发出通知");
            }
        },"t2").start();
    }
}
//t1   ---- come in
//t2   ---发出通知
//t1  ---被唤醒了

```

> `synchronized`总结：
>
> 1. wait和notify方法必须要在**同步**块或者方法里面，且**成对**出现使用
> 2. 先wait后notify才OK,**顺序不同会造成线程一直空等**
>
> **异常说明**
>
> 1. 去掉synchronized：会报错`IllegalMonitorStateException`
> 2. 把notify和wait的执行顺序对换：线程一直等待被唤醒



**Condition接口中的await后signal方法实现线程的等待和唤醒**

```java
public class LockSupportDemo
{
    public static void main(String[] args)
    {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();

        new Thread(() -> {
            lock.lock();
            try
            {
                System.out.println(Thread.currentThread().getName()+"\t-----come in");
                condition.await();
                System.out.println(Thread.currentThread().getName()+"\t -----被唤醒");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        },"t1").start();

        //暂停几秒钟线程
        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        new Thread(() -> {
            lock.lock();
            try
            {
                condition.signal();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
            System.out.println(Thread.currentThread().getName()+"\t"+"我要进行唤醒");
        },"t2").start();

    }
}
//t1  -----come in
//t2  我要进行唤醒
//t1   -----被唤醒

```

> 总结：
>
> - `await`和`notify`类似于上面`wait`和`notify`
>   - Condition中的线程等待和唤醒方法，需要先获取锁
>   - 一定要先await后signal，不能反了

>  **Object和Condition使用的限制条件**
>
> - 线程先要获得并持有锁，必须在锁块（synchronized或lock）中
> - 必须要先等待后唤醒，线程才能够被唤醒

 **LockSupport类中的park等待和unpark唤醒**

```java
public class LockSupportDemo
{
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"\t----------come in");
            LockSupport.park();
            System.out.println(Thread.currentThread().getName()+"\t----------被唤醒了");
        },"t1");
        t1.start();

        new Thread(()->{
            LockSupport.unpark(t1);
            System.out.println(Thread.currentThread().getName()+"\t-----发出通知，去唤醒t1");
        },"t2").start();
    }
}
//t1  ----------come in
//t2  -----发出通知，去唤醒t1
//t1  ----------被唤醒了

```

之前错误的先唤醒后等待，LockSupport照样支持

```java
public class LockSupportDemo
{
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            try {TimeUnit.SECONDS.sleep(3);} catch (InterruptedException e) {e.printStackTrace();}
            System.out.println(Thread.currentThread().getName()+"\t----------come in"+"\t"+System.currentTimeMillis());
            LockSupport.park();
            System.out.println(Thread.currentThread().getName()+"\t----------被唤醒了"+"\t"+System.currentTimeMillis());
        },"t1");
        t1.start();

        new Thread(()->{
            LockSupport.unpark(t1);
            System.out.println(Thread.currentThread().getName()+"\t-----发出通知，去唤醒t1");
        },"t2").start();
    }
}
//t2  -----发出通知，去唤醒t1
//t1  ----------come in  1654750785663
//t1  ----------被唤醒了  1654750785663

```

> sleep方法3秒后醒来，执行park无效，没有阻塞效果，解释如下。先执行了unpark(t1)导致上面的park方法形同虚设无效，**时间是一样的**
> \- 类似于高速公路的ETC，提前买好了通行证unpark，到闸机处直接抬起栏杆放行了，没有park拦截了。

**许可证只有一个**

```java
public class LockSupportDemo
{
    public static void main(String[] args) {
        Thread t1 = new Thread(()->{
            try {TimeUnit.SECONDS.sleep(3);} catch (InterruptedException e) {e.printStackTrace();}
            System.out.println(Thread.currentThread().getName()+"\t----------come in"+"\t"+System.currentTimeMillis());
            LockSupport.park();
            LockSupport.park();
            System.out.println(Thread.currentThread().getName()+"\t----------被唤醒了"+"\t"+System.currentTimeMillis());
        },"t1");
        t1.start();

        new Thread(()->{
            LockSupport.unpark(t1);
            LockSupport.unpark(t1);
            System.out.println(Thread.currentThread().getName()+"\t-----发出通知，去唤醒t1");
        },"t2").start();
    }
}
//t2  -----发出通知，去唤醒t1
//t1  ----------come in  1654750970677--------------------卡在这里了
```

> **总结：**
>
> * Lock Support是用来创建锁和其他同步类的基本线程阻塞原语。
> * Lock Support是一个线程阻塞工具类， 所有的方法都是静态方法， 可以让线程在任意位置阻塞， 阻塞之后也有对应的唤醒方法。归根结底， Lock Support调用的Unsafe中的native代码。
>
> * Lock Support提供park() 和unpark() 方法实现阻塞线程和解除线程阻塞的过程
> * Lock Support和每个使用它的线程都有一个许可(permit) 关联。
>   每个线程都有一个相关的permit， permit最多只有一个， 重复调用un park也不会积累凭证。	
>
> 形象的理解
>
> * 线程阻塞需要消耗凭证(permit) ， 这个凭证最多只有1个。
>   当调用方法`park`时,如果有凭证，则会直接消耗掉这个凭证然后正常退出；如果无凭证，就必须阻塞等待凭证可用；
>   而`unpark(threadName)`则相反， 它会为指定线程增加一个凭证， 但**凭证最多只能有1个， 累加无效。**

## JMM（java内存模型）

![img](juc%E5%AD%A6%E4%B9%A0.assets/v2-af520d543f0f4f205f822ec3b151ad46_r.jpg)

**概念：**JMM（Java内存模型）是Java虚拟机规范中定义的一套规则，用于描述多线程程序中共享变量的访问方式和交互规则。JMM定义了一组规则，确保在多线程环境中，各个线程之间能够正确地进行内存访问和交互，并且保证程序的正确性和可预测性。

关键点：可见性,原子性,有序性

1. **可见性**：当一个线程修改了共享变量的值后，其他线程应该能够立即看到这个变化。
2. **原子性**：对于一些需要原子性操作的变量，例如long和double类型，它们的读写应该是原子性的。一个线程的操作不能被其他线程干扰
3. **有序性**：程序执行的顺序应该符合开发者编写的程序的顺序，不能因为编译器和处理器的优化而导致程序出错。

```java
public void mySort()
{
    int x = 11; //语句1
    int y = 12; //语句2
    x = x + 5;  //语句3
    y = x * x;  //语句4
}
 
  1234
  2134
  1324
 
问题：请问语句4可以重排后变成第一个条吗？

```

> **有序性解释：**
>
> 对于于一个线程的执行代码而言，我们总是习惯性认为代码的执行总是从上到下，有序执行。但为了提供性能，编译器和处理器通常会对指令序列进行重新排序**。Java规范规定JVM线程内部维持顺序化语义，即只要程序的最终结果与它顺序化执行的结果相等，那么指令的执行顺序可以与代码顺序不一致，此过程叫指令的重排序。**

### JMM规范下，多线程对变量的读写过程

由于JVM运行程序的实体是线程，而每个线程创建时JVM都会为其创建一个工作内存(有些地方称为栈空间)，工作内存是每个线程的私有数据区域，而Java内存模型中规定所有变量都存储在主内存，主内存是共享内存区域，所有线程都可以访问，但线程对变量的操作(读取赋值等)必须在工作内存中进行，**首先要将变量从主内存拷贝到的线程自己的工作内存空间，然后对变量进行操作，操作完成后再将变量写回主内存，不能直接操作主内存中的变量，各个线程中的工作内存中存储着主内存中的变量副本拷贝**，因此不同的线程间无法访问对方的工作内存，线程间的通信(传值)必须通过主内存来完成，其简要访问过程如下图

![在这里插入图片描述](juc%E5%AD%A6%E4%B9%A0.assets/d44bcacbf1f648bc821481959610de73.png)

> **总结：**
>
> 1. 我们定义的所有共享变量都存储在物理主内存中
>
> 2. 每个线程都有自己独立的工作内存，里面保存该线程使用到的变量的副本（主内存中该变量的一份拷贝）
>
> 3. 线程对共享变量所有的操作都必须先在线程自己的工作内存中进行后写回主内存，不能直接从主内存中读写（不能越级）
>
> 4. 不同线程之间也无法直接访问其他线程的工作内存中的变量，线程间变量值的传递需要通过主内存来进行（同级不能相互访问）

### 多线程先行发生原则之happens-before

* 先行发生原则（happens-before）
  - 在JMM中，如果一个操作执行的结果需要对另一个操作可见性或者代码重新排序，那么这两个操作之间必须存在happens-before（先行发生）原则。逻辑上的**先后关系**。

**例子**

```markdown
x=5;线程A执行

y=x;线程B执行

问：y一定等于5吗？

答：不一定

如果线程A的操作（x= 5）happens-before(先行发生)线程B的操作（y = x）,那么可以确定线程B执行后y = 5 一定成立;

如果他们不存在happens-before原则，那么y = 5 不一定成立。

是happens-before原则的威力。-------------------》包含可见性和有序性的约束
```

* **总原则：**

如果一个操作happens-before另一个操作，那么第一个操作的执行结果将对第二个操作可见，而且第一个操作的执行顺序排在第二个操作之前。

两个操作之间存在happens-before关系，并不一定要按照happens-before原则制定的顺序来执行。如果重排序之后的执行结果与按照happens-before关系来执行的结果一致，那么这种重排序并不非法。

***

**8条原则**

1. 次序规则：**一个线程内**，按照代码顺序，写在前面的操作先行发生于写在后面的操作。
2. 锁定规则：一个解锁操作先行发生于之后对同一把锁的加锁操作。
3. volatile变量规则：对一个volatile变量的写操作先行发生于之后对该变量的读操作。前面的写对后面的读是**可见**的，这里的后面同样是指时间上的先后。
4. 传递性规则：如果操作A先行发生于操作B，而操作B又先行发生于操作C，则可以得出操作A先行发生于操作C

## 👨‍🚒volatile

**Volatile是Java中的一个关键字，用来修饰变量。当一个变量被声明为volatile时，它会具有以下特性：**

1. **可见性：**当一个线程修改了一个`volatile`变量的值时，这个修改操作会**立即刷新到主存**中，而不是仅仅更新该线程所在的CPU的缓存中，这样其他线程就可以立即看到这个修改。其他线程能够放弃自身关于`volatile`变量的副本，重新从主存中读取

2. **有序性：**在多线程程序中，重排序可能会导致一些意外的结果，因为指令重排序会影响程序的执行顺序，导致程序的执行结果与预期不符。**使用`volatile`可以保证编译器和CPU不会对`volatile`变量的读写指令进行重排序**，从而保证程序的执行顺序与代码的编写顺序一致。

### **🧚‍♀️可见性举例**

```java
int a = 0;
volatile bool flag = false;

public void write() {
   a = 2;              //1
   flag = true;        //2
}

public void read() {
   if (flag) {         //3
       int ret = a * a;//4
   }
}
```

> 以上代码，如果去除掉`volatile`关键字，那么在多线程并发的情况下可能就会出现问题，某个线程执行read（）方法，得到的ret可能为0，因为发生了重排序，还有可能就是flag信号值无法更新，导致线程进不去flag的判断。
>
> 因此我们使用volatile保证了代码不会重排序，并且修改volatile变量flag时，对于所有线程都是可见的。
>
> **当写一个volatile变量时，JMM会把该线程对应的本地内存中的共享变量刷新到主内存**
>
> **当读一个volatile变量时，JMM会把该线程对应的本地内存置为无效，线程接下来将从主内存中读取共享变量。**

### 为什么不能保证原子性

![11.png](juc%E5%AD%A6%E4%B9%A0.assets/ed94551988ab4d1bafb2951b0e24350dtplv-k3u1fbpfcp-zoom-in-crop-mark4536000.webp)

**只能对单个volatile变量的读或写具有原子性，对于类似volatile++这样的复合操作就无法保证原子性了。**

例如：

```java
public class Test {
    public volatile int inc = 0;
 
    public void increase() {
        inc++;
    }
 
    public static void main(String[] args) {
        final Test test = new Test();
        for(int i=0;i<10;i++){
            new Thread(){
                public void run() {
                    for(int j=0;j<1000;j++)
                        test.increase();
                };
            }.start();
        }
 
        while(Thread.activeCount()>1)  //保证前面的线程都执行完
            Thread.yield();
        System.out.println(test.inc);
    }
```

> **结果分析：**
>
> 按道理来说，结果应该为10000，但是运行起来结果很可能是小于10000的值。
>
> 解释：（inc++无法保证原子性）假设线程A，读取了inc的值为10，这时候被阻塞了，因为没有对变量进行修改，触发不了volatile规则。
>
> 线程B此时也读读inc的值，主存里inc的值依旧为10，做自增，然后立刻就被写回主存了，为11。
>
> 此时又轮到线程A执行，由于工作内存里保存的是10，所以继续做自增，再写回主存，11又被写了一遍。所以虽然两个线程执行了两次increase()，结果却只加了一次。
>
> 有人说，**volatile不是会使缓存行无效的吗**？但是这里线程A读取到线程B也进行操作之前，并没有修改inc值，所以线程B读取的时候，还是读的10。
>
> 又有人说，线程B将11写回主存，**不会把线程A的缓存行设为无效吗**？但是线程A的读取操作已经做过了啊，只有在做读取操作时，发现自己缓存行无效，才会去读主存的值，所以这里线程A只能继续做自增了。

> **综上所述：**这种复合操作的情景下，原子性的功能是维持不了了。但是volatile在上面那种设置flag值的例子里，由于对flag的读/写操作都是单步的，所以还是能保证原子性的。

### 如何禁止重排序

1. volatile有关禁止指令重排的行为

- **当第一个操作是 volatile 读时，不论第二个操作是什么，都不能重排序；这个操作保证了volatile读之后的操作不会被重排到volatile读之前**
- **当第二个操作为 volatile 写时，不论第一个操作是什么，都不能重排序；这个操作保证了volatile写之前的操作不会被重排到volatile写之后**
- **当第一个操作为 volatile 写时，第二个操作为 volatile 读时，不能重排序**

1. 四大内存屏障插入情况（具体见下面代码分析）

- 在每一个 volatile 写操作前面插入一个 **storestore** 屏障
- 在每一个 volatile 写操作后面插入一个 **storeload** 屏障
- 在每一个 volatile 读操作后面插入一个 **loadload** 屏障
- 在每一个 volatile 读操作后面插入一个 **loadstore** 屏障

### 内存屏障

* **粗分两种**

  1. **读屏障**

     * 在读指令之前插入读屏障，让工作内存或CPU高速缓存当中的缓存数据失效，重新回到主内存中获取最新数据。

     1. **写屏障**
        * 在写指令之后插入写屏障，强制把写缓冲区的数据刷回到主内存中

* **细分4种**

  1. **loadload：**
      读读，该屏障用来禁止处理器把上面的volatile读与下面的普通读重排序

  2. **storestore：**
      写写，该屏障可以保证在volatile写之前，其前面的所有普通写操作都已经刷新到主内存中

  3. **loadstore：**
      读写，该屏障用来禁止处理器把上面的volatile读与下面的普通写重排序

  4. **storeload：**
      写读，该屏障的作用是避免volatile与后面可能有的volatile读/写操作重排序

![image-20230219004302147](juc%E5%AD%A6%E4%B9%A0.assets/image-20230219004302147.png)

### 应用场景

1. **状态标志**
   使用到一个boolean类型的标志位上，这样一个线程改变了该标志位，其他所有线程都可见

```java
int a = 0;
volatile bool flag = false;

public void write() {
    a = 2;              //1
    flag = true;        //2
}

public void multiply() {
    if (flag) {         //3
        int ret = a * a;//4
    }
}
```

1. 单例模式的实现，典型的双重检查锁定（DCL）

```java
public class SafeDoubleCheckSingleton
{
    //通过volatile声明，实现线程安全的延迟初始化。
    private volatile static SafeDoubleCheckSingleton singleton;
    //私有化构造方法
    private SafeDoubleCheckSingleton(){
    }
    //双重锁设计
    public static SafeDoubleCheckSingleton getInstance(){
        if (singleton == null){
            //1.多线程并发创建对象时，会通过加锁保证只有一个线程能创建对象
            synchronized (SafeDoubleCheckSingleton.class){
                if (singleton == null){
                    //隐患：多线程环境下，由于重排序，该对象可能还未完成初始化就被其他线程读取
                                      //原理:利用volatile，禁止 "初始化对象"(2) 和 "设置singleton指向内存空间"(3) 的重排序
                    singleton = new SafeDoubleCheckSingleton();
                }
            }
        }
        //2.对象创建完毕，执行getInstance()将不需要获取锁，直接返回创建对象
        return singleton;
    }
}
```

懒汉式，使用时才创建对象，而且为了避免初始化操作的指令重排序，给instance加上了volatile。

## 👨‍🏭CAS

**概念：**

CAS属于乐观锁，在使用 CAS 进行操作时，会先读取共享变量的当前值，然后比较当前值和预期值是否相同，如果相同，就将共享变量更新为新值。如果不相同，则说明其他线程已经修改了共享变量，当前线程的操作失败，需要重新读取当前值进行重试（**自旋**）。

### CAS分析

* 问题：当我们在并发环境下想保证i++的原子性，会用到`synchronized`关键字进行使用

```java
public class Test {

    public volatile int i;
   
    public int get(){
        return i;
    }
    public synchronized void add() {
        i++;
    }
}
```

使用上述的方法，因为`synchronized`属于重量级锁，因此性能会差一些。我们可以使用`AtomicInteger`,就可以保证`i`原子的`++`了。性能也会优于`synchronized`

```java
public class Test {

    public AtomicInteger i;

    public void add() {
        i.getAndIncrement();
    }
}
```

**查看`getAndIncrement`（）方法的内部**

```java
public final int getAndIncrement() {
    return unsafe.getAndAddInt(this, valueOffset, 1);
}
```

> **Unsafe类说明：**
>
> * Unsafe 类是 Java 提供的一个非常底层的类，它提供了一些直接操作内存和线程的方法，是 Java 中实现高性能、并发、低级别操作的重要工具。但是，Unsafe 类的使用需要非常小心和谨慎，因为它可以直接访问和修改内存，如果使用不当可能会导致内存泄漏、数据损坏、线程安全等问题。

**继续查看`getAndAddInt`**

```java
public final int getAndAddInt(Object var1, long var2, int var4) {
    int var5;
    do {
        var5 = this.getIntVolatile(var1, var2);
    } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));

    return var5;
}
```

> **分析：**
>
> * 这里调用Unsafe类的getIntVolatile方法，该方法是一个本地方法，valueOffset代表value的偏移量，获取在`var1`中，偏移量为`var2`处的值。var1其实就是AtomicInteger，var2为valueOffset。这样就可以获取到内存中位于valueOffset处的值。
> * compareAndSwapInt(var1, var2, var5, var5 + var4)相当于是compareAndSwapInt（obj, offset, expect, update），他做的事为，如果obj内的value和期望值（expect）相等，那就说明没有其他线程修改过该变量，此时就可以将它更新为update，如果发现obj内的value和期望值（expect）不相等，**那么就采用自旋操作继续进行CAS操作（该步骤是借助CPU底层实现的，因此具有原子性）。**

### CAS存在的问题

1. **ABA问题**

> **分析：**
>
> * CAS在进行操作的时候，会先检查值有没有发生变化，如果值没有发生变化就更新，但是，如果一个变量的值，原来是A，另一个线程T1读取到值为A，，被一个线程T2操作成了B，又变回了A，此时T1 CAS操作仍然是成功的，但是在变量从A->B->A的过程中，是存在安全隐患的。由此我们可以看出，CAS只能判断值是否被修改过，无法判断其是否被中间的修改操作干扰过。这就是**ABA**问题
>
> **解决：**
>
> * 为了解决 ABA 问题，可以使用版本号或时间戳等机制来记录共享变量的修改次数或修改时间，从而增加 CAS 操作的安全性和可靠性。例如，每次对共享变量进行修改时，同时记录一个版本号或时间戳，CAS 操作需要比较和替换两个值（变量值和版本号），只有两个值都相同时才执行更新操作，这样就可以避免 ABA 问题的发生。AtomicStampedReference 类就是解决ABA问题而诞生的。

2. **循环时间长开销大**

上面我们说过如果CAS不成功，则会原地自旋，如果长时间自旋会给CPU带来非常大的执行开销。

### demo

**自旋锁的实现**

```JAVA
public class SpinLock {
    public AtomicReference<Thread> reference= new AtomicReference<>();
    public  void lock(){
        /*
        该方法是不停的循环等待尝试更新值，如果原本的值和期望值不同就会锁住，之后不断的while循环进行自旋尝试
         */
        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName()+"进来了lock");
        while (!reference.compareAndSet(null, thread)) {

        }
    }
    public  void unlock(){
        Thread thread = Thread.currentThread();
        reference.compareAndSet(thread,null);
        System.out.println(Thread.currentThread().getName()+"解锁");
    }
    public static void main(String[] args) {
        SpinLock spinLock = new SpinLock();
        new Thread(()->{
            spinLock.lock();
            try { Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
            spinLock.unlock();
        },"A").start();
        try { Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
        new Thread(()->{
            spinLock.lock();
            spinLock.unlock();
        },"B").start();
    }
```

**ABA问题展示**

```java
public class ABADemo {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(100);
        new Thread(()->{
            int value = atomicInteger.get();
            System.out.println(Thread.currentThread().getName()+"\t 首次获取值："+value);
            try { Thread.sleep(10);} catch (InterruptedException e) {e.printStackTrace();}
            atomicInteger.compareAndSet(100,101);
            atomicInteger.compareAndSet(101,100);
        },"A").start();
        
        new Thread(()->{
            int value = atomicInteger.get();
            System.out.println(Thread.currentThread().getName()+"\t 首次获取值："+value);
            try { Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
            atomicInteger.compareAndSet(100,105);
            System.out.println(Thread.currentThread().getName()+"最后的值："+atomicInteger.get());
        },"B").start();
    }
}
```

## 原子类（Atomic）

**概念：**Atomic类是Java中提供的一组线程安全的原子操作类，可以在多线程的环境下保证操作的原子性。比`synchronized`轻量，底层使用的是CAS

### 基本类型原子类

1. AtomicInteger：用于对int类型变量进行原子操作。
2. AtomicLong：用于对long类型变量进行原子操作。
3. AtomicBoolean：用于对boolean类型变量进行原子操作。

**例子：**

```java
class Number{
    int num=0;
    AtomicInteger atomicInteger=new AtomicInteger(num);
    public int getNum(){
        return atomicInteger.get();
    }
    public void add(){
        atomicInteger.getAndIncrement();
    }
}
public class AtomicIntegerDemo {
    public static void main(String[] args) throws InterruptedException {
        Number number = new Number();
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                for (int j = 0; j < 100; j++) {
                    number.add();
                }
                countDownLatch.countDown();
            },"thread name").start();
        }
        countDownLatch.await();
        System.out.println("最后结果："+number.atomicInteger.get());
    }
}
```

#### CountDownLatch说明

**概念：**`CountDownLatch`是Java中提供的一种同步辅助工具，位于`java.util.concurrent`包中。它允许一个或多个线程等待，直到在其他线程中执行的一组操作完成为止。，**它以给定的计数初始化，然后该计数会被一些线程递减，以表示它们完成了某些操作。正在等待这些操作完成的线程可以使用`await()`方法阻塞，直到计数达到零为止。**

### 数组类型原子类

1. AtomicIntegerArray
2. AtomicLongArray
3. AtomicReferenceArray

**原理和基本类型原子类一样**

```java
public class AtomicIntegerArrayDemo
{
    public static void main(String[] args)
    {
        AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(new int[5]);//0 0 0 0 0
        //AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(5);
        //AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(new int[]{1,2,3,4,5});//1 2 3 4 5 

        for (int i = 0; i <atomicIntegerArray.length(); i++) {
            System.out.println(atomicIntegerArray.get(i));
        }
        int tmpInt = 0;

        tmpInt = atomicIntegerArray.getAndSet(0,1122);//该方法返回旧的值
        System.out.println(tmpInt+"\t"+atomicIntegerArray.get(0));
        atomicIntegerArray.getAndIncrement(1);
        atomicIntegerArray.getAndIncrement(1);
        tmpInt = atomicIntegerArray.getAndIncrement(1);//该方法返回旧的值
        System.out.println(tmpInt+"\t"+atomicIntegerArray.get(1));
    }
}

```

### 引用类型原子类

1. AtomicReference 

2. AtomicStampedReference 

   * 该类可以解决CAS问题中的ABA问题，利用版本号和值都要相等来解决。**主要解决的问题是修改过几次**

   * ```java
     public class AtomicStampedReferenceDemo {
         static AtomicStampedReference<Integer> reference = new AtomicStampedReference<>(100,1);
         public static void main(String[] args) {
             new Thread(() -> {
                 int stamp = reference.getStamp();
                 System.out.println(Thread.currentThread().getName() + "\t 首次版本号:" + stamp);//1-----------初始获得一样的版本号
                 //暂停500毫秒，保证t4线程初始化拿到的版本号和我一样,
                try { Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
                 reference.compareAndSet(100, 126, reference.getStamp(), reference.getStamp() + 1);
                 System.out.println(Thread.currentThread().getName() + "\t 2次版本号:" + reference.getStamp());
                 reference.compareAndSet(126, 100, reference.getStamp(), reference.getStamp() + 1);
                 System.out.println(Thread.currentThread().getName() + "\t 3次版本号:" + reference.getStamp());
             }, "t3").start();
     
             new Thread(() -> {
                 int stamp = reference.getStamp();//记录一开始的版本号，并且写死
                 System.out.println(Thread.currentThread().getName() + "\t 首次版本号:" + stamp);//1------------初始获得一样的版本号
                 //暂停1秒钟线程，等待上面的t3线程，发生了ABA问题
                 try { Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
                 boolean result = reference.compareAndSet(100, 2019, stamp, stamp + 1);//这个还是初始的版本号，但是实际上版本号被T3修改了，所以肯定会失败
                 System.out.println(Thread.currentThread().getName() + "\t" + result + "\t" + reference.getReference());
             }, "t4").start();
         }
     }
     ```

3. AtomicMarkableReference

   * 该类和AtomicStampedReference 类似，不过不同的是，它解决的问题是**一次性的**，解决变量是否被修改过。

   * ```java
     public class AtomicMarkableDemo {
         public static void main(String[] args) {
             AtomicMarkableReference markableReference=new AtomicMarkableReference(100,false);
             new Thread(()->{
                 boolean marked = markableReference.isMarked();//是否被修改过
                 System.out.println(Thread.currentThread().getName()+"\t 刚开始"+marked);
                 try { Thread.sleep(200);} catch (InterruptedException e) {e.printStackTrace();}
                 markableReference.compareAndSet(100,200,marked,!marked);
             },"A").start();
             new Thread(()->{
                 boolean marked = markableReference.isMarked();//是否被修改过
                 System.out.println(Thread.currentThread().getName()+"\t 刚开始"+marked);
                 try { Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
                 markableReference.compareAndSet(100,200,marked,!marked);
                 System.out.println(Thread.currentThread().getName()+"\t 结果"+markableReference.isMarked()+"\t"+markableReference.getReference());
             },"B").start();
         }
     }
     ```

### 对象属性修改原子类



**说明：**能够对对象的成员变量属性进行原子操作。相比较与引用类型原子类，粒度更细

1. AtomicReferenceFieldUpdater：用于对某个类的引用类型成员变量进行原子操作

   * ```java
     public class AtomicReferenceFiledUpdatedDemo {
     
         // 定义一个Person类，包含两个属性name和age
         static class Person {
             volatile String name;
             volatile int age;
     
             public Person(String name, int age) {
                 this.name = name;
                 this.age = age;
             }
     
             @Override
             public String toString() {
                 return "Person{" +
                         "name='" + name + '\'' +
                         ", age=" + age +
                         '}';
             }
         }
     
         // 定义一个AtomicReferenceFieldUpdater对象，用于对Person对象的name属性进行原子更新
         static AtomicReferenceFieldUpdater<Person, String> nameUpdater =
                 AtomicReferenceFieldUpdater.newUpdater(Person.class, String.class, "name");
     
         public static void main(String[] args) throws InterruptedException {
             // 创建一个Person对象，并输出其初始值
             Person person = new Person("张三", 18);
             System.out.println("修改前：" + person);
             CountDownLatch countDownLatch = new CountDownLatch(10);
             // 创建10个线程，并启动它们
             for (int i = 0; i < 10; i++) {
                 new Thread(() -> {
                     // 在每个线程中使用nameUpdater对象对person对象的name属性进行原子更新
                     boolean success = nameUpdater.compareAndSet(person, "张三", "李四");
                     if (success) {
                         System.out.println(Thread.currentThread().getName() + " 修改成功：" + person);
                     } else {
                         System.out.println(Thread.currentThread().getName() + " 修改失败：" + person);
                     }
                     countDownLatch.countDown();
                 }).start();
             }
     
             // 等待所有线程执行完成
     //        Thread.sleep(1000);
             countDownLatch.await();
     
             // 输出最终结果
             System.out.println("修改后：" + person);
         }
     }
     ```

2. AtomicIntegerFieldUpdater：于对某个类的int类型成员变量进行原子操作

   * ```java
     class Cat {
        public volatile int age;
         AtomicIntegerFieldUpdater<Cat> fieldUpdater=AtomicIntegerFieldUpdater.newUpdater(Cat.class,"age");
     }
     public class AtomicIntegerFiledUpdaterDemo {
         public static void main(String[] args) throws InterruptedException {
             Cat cat = new Cat();
             System.out.println("最出结果:"+cat.fieldUpdater.get(cat));
             CountDownLatch countDownLatch = new CountDownLatch(10);
             for (int i = 0; i < 10; i++) {
                 new Thread(()->{
                     //每个线程增加一岁
                     int oldValue = cat.fieldUpdater.getAndIncrement(cat);
                     System.out.println(Thread.currentThread().getName()+"\t 更新之前："+oldValue);
                     System.out.println(Thread.currentThread().getName()+"\t 更新之后："+cat.fieldUpdater.get(cat));
                     countDownLatch.countDown();
                 },String.valueOf(i)).start();
             }
             countDownLatch.await();
             System.out.println("最终结果:"+cat.fieldUpdater.get(cat));
         }
     }
     ```

3. AtomicLongFieldUpdater：用于对某个类的long类型成员变量进行原子操作。

**和上面的类似**

> **==注意==：**
>
> 1. **要修改的变量必须是`public volatile`**
> 2. 创建更新器（updater）使用类的静态方法`newUpdater`,同时里面的属性为，要修改的类的Class，要修改成员变量属性Class，属性名称

## 🤴ThreadLocal

**概念：**线程本地变量。如果你创建了一个`ThreadLocal`变量，那么访问这个变量的每个线程都会有这个变量的一个本地拷贝，多个线程操作这个变量的时候，实际是在操作自己本地内存里面的变量，从而起到**线程隔离**的作用，避免了并发场景下的线程安全问题。

```java
//创建一个ThreadLocal变量
static ThreadLocal<String> localVariable = new ThreadLocal<>();
```

### 为什么使用ThreadLocal

并发场景下，会存在多个线程同时修改一个共享变量的场景。这就可能会出现**线性安全问题**。

为了解决线性安全问题，可以用加锁的方式，比如使用`synchronized` 或者`Lock`。但是加锁的方式，可能会导致系统变慢。加锁示意图如下：

![img](juc%E5%AD%A6%E4%B9%A0.assets/56bf1ff5857042c785310ede9501b154tplv-k3u1fbpfcp-zoom-in-crop-mark4536000.webp)

还有一种方法就是使用`ThreadLocal`,使用`ThreadLocal`类访问共享变量时，会在每个线程的本地，都保存一份共享变量的拷贝副本。多线程对共享变量修改时，实际上操作的是这个变量副本，从而保证线性安全。

![img](juc%E5%AD%A6%E4%B9%A0.assets/e4f0615e9ff3409d90babd8e90d376a7tplv-k3u1fbpfcp-zoom-in-crop-mark4536000.webp)

### 原理分析

**ThreadLocal内存结构图**

![img](juc%E5%AD%A6%E4%B9%A0.assets/ed5978a54046419897b1f5a039889931tplv-k3u1fbpfcp-zoom-in-crop-mark4536000.webp)

从内存结构图，我们可以看到：

- `Thread`类中，有个`ThreadLocal.ThreadLocalMap`  的成员变量。
- `ThreadLocalMap`内部维护了`Entry`数组，每个`Entry`代表一个完整的对象，`key`是`ThreadLocal`本身，`value`是`ThreadLocal`的泛型对象值。

#### 源码分析

对照着几段关键源码来看，更容易理解一点哈~我们回到`Thread`类源码，可以看到成员变量`ThreadLocalMap`的初始值是为`null`

```java
public class Thread implements Runnable {
   //ThreadLocal.ThreadLocalMap是Thread的属性
   ThreadLocal.ThreadLocalMap threadLocals = null;
}
```

`ThreadLocalMap`的关键源码如下：

```java
static class ThreadLocalMap {
    
    static class Entry extends WeakReference<ThreadLocal<?>> {
        /** The value associated with this ThreadLocal. */
        Object value;

        Entry(ThreadLocal<?> k, Object v) {
            super(k);
            value = v;
        }
    }
    //Entry数组
    private Entry[] table;
    
    // ThreadLocalMap的构造器，ThreadLocal作为key
    ThreadLocalMap(ThreadLocal<?> firstKey, Object firstValue) {
        table = new Entry[INITIAL_CAPACITY];
        int i = firstKey.threadLocalHashCode & (INITIAL_CAPACITY - 1);
        table[i] = new Entry(firstKey, firstValue);
        size = 1;
        setThreshold(INITIAL_CAPACITY);
    }
}
```

`ThreadLocal`类中的关键`set()`方法：

```java
 public void set(T value) {
        Thread t = Thread.currentThread(); //获取当前线程t
        ThreadLocalMap map = getMap(t);  //根据当前线程获取到ThreadLocalMap
        if (map != null)  //如果获取的ThreadLocalMap对象不为空
            map.set(this, value); //K，V设置到ThreadLocalMap中
        else
            createMap(t, value); //创建一个新的ThreadLocalMap
    }
    
     ThreadLocalMap getMap(Thread t) {
       return t.threadLocals; //返回Thread对象的ThreadLocalMap属性
    }

    void createMap(Thread t, T firstValue) { //调用ThreadLocalMap的构造函数
        t.threadLocals = new ThreadLocalMap(this, firstValue); this表示当前类ThreadLocal
    }
    
```

`ThreadLocal`类中的关键`get()`方法

```java
    public T get() {
        Thread t = Thread.currentThread();//获取当前线程t
        ThreadLocalMap map = getMap(t);//根据当前线程获取到ThreadLocalMap
        if (map != null) { //如果获取的ThreadLocalMap对象不为空
            //由this（即ThreadLoca对象）得到对应的Value，即ThreadLocal的泛型值
            ThreadLocalMap.Entry e = map.getEntry(this);
            if (e != null) {
                @SuppressWarnings("unchecked")
                T result = (T)e.value; 
                return result;
            }
        }
        return setInitialValue(); //初始化threadLocals成员变量的值
    }
    
     private T setInitialValue() {
        T value = initialValue(); //初始化value的值
        Thread t = Thread.currentThread(); 
        ThreadLocalMap map = getMap(t); //以当前线程为key，获取threadLocals成员变量，它是一个ThreadLocalMap
        if (map != null)
            map.set(this, value);  //K，V设置到ThreadLocalMap中
        else
            createMap(t, value); //实例化threadLocals成员变量
        return value;
    }
```

> **总结：**
>
> 1. `Thread`线程类有一个类型为`ThreadLocal.ThreadLocalMap`的实例变量`threadLocals`，即每个线程都有一个属于自己的`ThreadLocalMap`。
> 2. `ThreadLocalMap`内部维护着`Entry`数组，每个`Entry`代表一个完整的对象，`key`是`ThreadLocal`本身，`value`是`ThreadLocal`的泛型值。
> 3. 并发多线程场景下，每个线程`Thread`，在往`ThreadLocal`里设置值的时候，都是往自己的`ThreadLocalMap`里存，读也是以某个`ThreadLocal`作为引用，在自己的`map`里找对应的`key`，从而可以实现了**线程隔离**。

### 为什么不使用线程id作为ThreadLocalMap的key？

```java
public class TianLuoThreadLocalTest {

    private static final ThreadLocal<String> threadLocal1 = new ThreadLocal<>();
    private static final ThreadLocal<String> threadLocal2 = new ThreadLocal<>();
 
}
```

这种场景：一个使用类，有两个共享变量，也就是说用了两个`ThreadLocal`成员变量的话。如果用线程`id`作为`ThreadLocalMap`的`key`，怎么区分哪个`ThreadLocal`成员变量呢？

![img](juc%E5%AD%A6%E4%B9%A0.assets/d440062943254b2c9b93cebc3b4f155atplv-k3u1fbpfcp-zoom-in-crop-mark4536000.webp)

### ThreadLocal为什么会导致内存泄漏

TreadLocal的引用示意图

![img](juc%E5%AD%A6%E4%B9%A0.assets/47ef8e97d7aa457cb8e57920ef2fbe9ftplv-k3u1fbpfcp-zoom-in-crop-mark4536000.webp)

`ThreadLocalMap`使用`ThreadLocal`的**弱引用**作为`key`，当`ThreadLocal`变量被手动设置为`null`，即一个`ThreadLocal`没有外部强引用来引用它，当系统GC时，`ThreadLocal`一定会被回收。这样的话，`ThreadLocalMap`中就会出现`key`为`null`的`Entry`，就没有办法访问这些`key`为`null`的`Entry`的`value`，如果当前线程再迟迟不结束的话(比如线程池的核心线程)，这些`key`为`null`的`Entry`的`value`就会一直存在一条强引用链：Thread变量 -> Thread对象 -> ThreaLocalMap -> Entry -> value -> Object 永远无法回收，造成内存泄漏。

**当ThreadLocal变量被手动设置为`null`后的引用链图**

![img](juc%E5%AD%A6%E4%B9%A0.assets/830268d25374420e8fb037a82a82c820tplv-k3u1fbpfcp-zoom-in-crop-mark4536000.webp)

实际上，`ThreadLocalMap`的设计中已经考虑到这种情况。所以也加上了一些防护措施：即在`ThreadLocal`的`get`,`set`,`remove`方法，都会清除线程`ThreadLocalMap`里所有`key`为`null`的`value`。

`ThreadLocalMap`的`set`方法：

```java
  private void set(ThreadLocal<?> key, Object value) {

      Entry[] tab = table;
      int len = tab.length;
      int i = key.threadLocalHashCode & (len-1);

      for (Entry e = tab[i];
            e != null;
            e = tab[i = nextIndex(i, len)]) {
          ThreadLocal<?> k = e.get();

          if (k == key) {
              e.value = value;
              return;
          }

           //如果k等于null,则说明该索引位之前放的key(threadLocal对象)被回收了,这通常是因为外部将threadLocal变量置为null,
           //又因为entry对threadLocal持有的是弱引用,一轮GC过后,对象被回收。
            //这种情况下,既然用户代码都已经将threadLocal置为null,那么也就没打算再通过该对象作为key去取到之前放入threadLocalMap的value, 因此ThreadLocalMap中会直接替换调这种不新鲜的entry。
          if (k == null) {
              replaceStaleEntry(key, value, i);
              return;
          }
        }

        tab[i] = new Entry(key, value);
        int sz = ++size;
        //触发一次Log2(N)复杂度的扫描,目的是清除过期Entry  
        if (!cleanSomeSlots(i, sz) && sz >= threshold)
          rehash();
    }
```

ThreadLocal的`get`方法：

```java
  public T get() {
    Thread t = Thread.currentThread();
    ThreadLocalMap map = getMap(t);
    if (map != null) {
        //去ThreadLocalMap获取Entry，方法里面有key==null的清除逻辑
        ThreadLocalMap.Entry e = map.getEntry(this);
        if (e != null) {
            @SuppressWarnings("unchecked")
            T result = (T)e.value;
            return result;
        }
    }
    return setInitialValue();
}

private Entry getEntry(ThreadLocal<?> key) {
        int i = key.threadLocalHashCode & (table.length - 1);
        Entry e = table[i];
        if (e != null && e.get() == key)
             return e;
        else
          //里面有key==null的清除逻辑
          return getEntryAfterMiss(key, i, e);
    }
        
private Entry getEntryAfterMiss(ThreadLocal<?> key, int i, Entry e) {
        Entry[] tab = table;
        int len = tab.length;

        while (e != null) {
            ThreadLocal<?> k = e.get();
            if (k == key)
                return e;
            // Entry的key为null,则表明没有外部引用,且被GC回收,是一个过期Entry
            if (k == null)
                expungeStaleEntry(i); //删除过期的Entry
            else
                i = nextIndex(i, len);
            e = tab[i];
        }
        return null;
    }
```

###  key是弱引用，GC回收会影响ThreadLocal的正常工作嘛？

`ThreadLocal`的`key`既然是**弱引用**.会不会GC贸然把`key`回收掉，进而影响`ThreadLocal`的正常使用？

> * **弱引用**:具有弱引用的对象拥有更短暂的生命周期。如果一个对象只有弱引用存在了，则下次GC**将会回收掉该对象**（不管当前内存空间足够与否）

不会的，因为有`ThreadLocal变量`引用着它，是不会被GC回收的，除非手动把`ThreadLocal变量设置为null`，我们可以跑个demo来验证一下：

```java
  public class WeakReferenceTest {
    public static void main(String[] args) {
        Object object = new Object();
        WeakReference<Object> testWeakReference = new WeakReference<>(object);
        System.out.println("GC回收之前，弱引用："+testWeakReference.get());
        //触发系统垃圾回收
        System.gc();
        System.out.println("GC回收之后，弱引用："+testWeakReference.get());
        //手动设置为object对象为null
        object=null;
        System.gc();
        System.out.println("对象object设置为null，GC回收之后，弱引用："+testWeakReference.get());
    }
}
运行结果：
GC回收之前，弱引用：java.lang.Object@7b23ec81
GC回收之后，弱引用：java.lang.Object@7b23ec81
对象object设置为null，GC回收之后，弱引用：null
```

### ThreadLocal内存泄漏的问题

内存泄漏的例子，其实就是用线程池，一直往里面放对象

```java
public class ThreadLocalTestDemo {

    private static ThreadLocal<TianLuoClass> tianLuoThreadLocal = new ThreadLocal<>();


    public static void main(String[] args) throws InterruptedException {

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 5, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>());

        for (int i = 0; i < 10; ++i) {
            threadPoolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("创建对象：");
                    TianLuoClass tianLuoClass = new TianLuoClass();
                    tianLuoThreadLocal.set(tianLuoClass);
                    tianLuoClass = null; //将对象设置为 null，表示此对象不在使用了
                   // tianLuoThreadLocal.remove();
                }
            });
            Thread.sleep(1000);
        }
    }

    static class TianLuoClass {
        // 100M
        private byte[] bytes = new byte[100 * 1024 * 1024];
    }
}


创建对象：
创建对象：
创建对象：
创建对象：
Exception in thread "pool-1-thread-4" java.lang.OutOfMemoryError: Java heap space
	at com.example.dto.ThreadLocalTestDemo$TianLuoClass.<init>(ThreadLocalTestDemo.java:33)
	at com.example.dto.ThreadLocalTestDemo$1.run(ThreadLocalTestDemo.java:21)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)
	at java.lang.Thread.run(Thread.java:748)
```

运行结果出现了OOM，`tianLuoThreadLocal.remove();`加上后，则不会`OOM`。

我们这里**没有手动设置**`tianLuoThreadLocal`变量为`null`，但是还是**会内存泄漏**。因为我们使用了线程池，线程池有很长的生命周期，因此线程池会一直持有`tianLuoClass`对象的`value`值，即使设置`tianLuoClass = null;`引用还是存在的。这就好像，你把一个个对象`object`放到一个`list`列表里，然后再单独把`object`设置为`null`的道理是一样的，列表的对象还是存在的。

### Entry的key为什么要设置成弱引用

* **强引用**:我们平时`new`了一个对象就是强引用，例如` Object obj = new Object();`即使在内存不足的情况下，JVM宁愿抛出OutOfMemory错误也不会回收这种对象。

* **软引用**：如果一个对象只具有软引用，则内存空间足够，垃圾回收器就不会回收它；如果内存空间不足了，就会回收这些对象的内存。

* **弱引用**:具有弱引用的对象拥有更短暂的生命周期。如果一个对象只有弱引用存在了，则下次GC**将会回收掉该对象**（不管当前内存空间足够与否）。

* **虚引用**:如果一个对象仅持有虚引用，那么它就和没有任何引用一样，在任何时候都可能被垃圾回收器回收。虚引用主要用来跟踪对象被垃圾回收器回收的活动。。

![img](juc%E5%AD%A6%E4%B9%A0.assets/c1925a60abe14a0aa91bd538e131cceftplv-k3u1fbpfcp-zoom-in-crop-mark4536000.webp)

- 如果`Key`使用强引用：当`ThreadLocal`的对象被回收了，但是`ThreadLocalMap`还持有`ThreadLocal`的强引用的话，如果没有手动删除，ThreadLocal就不会被回收，会出现Entry的内存泄漏问题。
- 如果`Key`使用弱引用：当`ThreadLocal`的对象被回收了，因为`ThreadLocalMap`持有ThreadLocal的弱引用，即使没有手动删除，ThreadLocal也会被回收。`value`则在下一次`ThreadLocalMap`调用`set,get，remove`的时候会被清除。

因此可以发现，使用弱引用作为`Entry`的`Key`，可以多一层保障：弱引用`ThreadLocal`不会轻易内存泄漏，对应的`value`在下一次`ThreadLocalMap`调用`set,get,remove`的时候会被清除。

实际上，我们的内存泄漏的根本原因是，不再被使用的`Entry`，没有从线程的`ThreadLocalMap`中删除。一般删除不再使用的`Entry`有这两种方式：

- 一种就是，使用完`ThreadLocal`，手动调用`remove()`，把`Entry从ThreadLocalMap`中删除
- 另外一种方式就是：`ThreadLocalMap`的自动清除机制去清除过期`Entry`.（`ThreadLocalMap`的`get(),set()`时都会触发对过期`Entry`的清除）

## 👩🏻‍🌾Synchronized锁升级

[]:https://blog.csdn.net/DBC_121/article/details/105453101

![在这里插入图片描述](juc%E5%AD%A6%E4%B9%A0.assets/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0RCQ18xMjE=,size_16,color_FFFFFF,t_70.png)

### 偏向锁

1. **锁的获取：**

   * 当一个线程访问同步代码块并获取锁时，会在锁对象的**对象头**中的Mark Word，将该线程的线程id记录下来，并将Mark Word设置为偏向锁的标记，此时锁对象为偏向锁。

2. **锁的使用：**

   * 当一个线程再次访问锁对象，它会发现该对象的Mark Word中的偏向锁标记已经被设置了，并且记录的线程ID就是自己的ID，那么该线程就可以直接获取对象的锁，无需再次竞争。

3. **锁竞争：**

   * 当一个线程尝试获取一个已经有偏向锁标记的对象的锁时，它会发现该对象的Mark Word中的偏向锁标记已经被设置了，并且记录的线程ID不是自己的ID。此时线程会通过CAS操作竞争锁，如果竞争成功则将Mark Word中线程ID设置为当前线程ID，**如果CAS获取偏向锁失败，则表示有竞争，当到达全局安全点（safepoint）时获得偏向锁的线程被挂起，偏向锁升级为轻量级锁**，然后被阻塞在安全点的线程继续往下执行同步代码。

4. **撤销锁：**

   * 关于偏向锁的撤销，需要等待全局安全点，即在某个时间点上没有字节码正在执行时，它会先暂停拥有偏向锁的线程，然后判断锁对象是否处于被锁定状态。如果线程不处于活动状态，则将对象头设置成无锁状态，并撤销偏向锁，恢复到无锁（标志位为01）或轻量级锁（标志位为00）的状态。

   * > **此时会出现两种情况**
     >
     > 1. 第一个线程正在执行`Synchronized`方法（在同步块中），他还没有执行完成，就有其他线程来争夺，该偏向锁会被取消掉并出现**==锁升级==**。此时轻量级锁由原持有偏向锁的线程持有，继续执行齐同步代码，而正在竞争的线程会进入自旋等待获取该轻量级锁
     > 2. 第一个线程执行完成`Synchronized`方法（退出了同步代码块），则将对象头设置成无所状态并撤销偏向锁，线程重新CAS争夺，重新偏向

![image-20230221160422401](juc%E5%AD%A6%E4%B9%A0.assets/image-20230221160422401.png)

> **总结：**
>
> 偏向锁的撤销，需要等待**全局安全点**（在这个时间点上没有正在执行的字节码）。它会首先暂停拥有偏向锁的线程，然后检查持有偏向锁的线程是否活着，如果线程不处于活动状态，则将对象头设置成无锁状态；如果线程仍然活着，拥有偏向锁的栈会被执行，遍历偏向对象的锁记录，栈中的锁记录和对象头的Mark Word**要么**重新偏向于其他线程，**要么**恢复到无锁或者标记对象不适合作为偏向锁，最后唤醒暂停的线程。

![在这里插入图片描述](juc%E5%AD%A6%E4%B9%A0.assets/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RaODQ1MTk1NDg1,size_16,color_FFFFFF,t_70.png)

### 轻量级锁

1. **加锁：**

   * 在代码进入同步块的时候，如果同步对象锁状态为偏向状态（就是锁标志位为“01”状态，是否为偏向锁标志位为“1”），虚拟机首先将在当前线程的栈帧中建立一个名为**锁记录（Lock Record）**的空间，**用于存储锁对象目前的Mark Word的拷贝**。官方称之为 Displaced Mark Word（所以这里我们认为Lock Record和 Displaced Mark Word其实是同一个概念），**并且将对象头中的Mark Word复制到锁记录中**

     ![在这里插入图片描述](juc学习.assets/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0RCQ18xMjE=,size_16,color_FFFFFF,t_70-167697548700927-167697550639030.png)

   * CAS尝试将对象头中的Mark Word更新为指向Lock Record的指针，**如果成功，则代表当前线程获得锁，并且对象Mark Word的锁标志位设置为“00”，即表示此对象处于轻量级锁定状态**

     ![在这里插入图片描述](https://gitee.com/xiongshengyu/juclearning/raw/master/img/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0RCQ18xMjE=,size_16,color_FFFFFF,t_70-167697558277733.png)

   * **如果失败，代表其他线程获取到锁，当前线程尝试自旋来获取锁。**如果自旋一定次数之后，仍然没有获取到锁，说明竞争很严重，升级为重量级锁

2. **解锁：**

   * 通过CAS操作尝试把线程中复制的Displaced Mark Word对象替换当前的Mark Word。
   * 如果替换成功，整个同步过程就完成了。
   * 如果替换失败，说明有其他线程尝试过获取该锁（此时锁已膨胀），那就要在释放锁的同时，唤醒被挂起的线程。

### 重量级锁

* 调用omAlloc分配一个`ObjectMonitor`对象，把锁对象头的mark word锁标志位变成 “10 ”，然后在mark word存储指向`ObjectMonitor`对象的指针

* `ObjectMonitor`中有两个队列，``_WaitSet`和`_EntryList`，用来保存`ObjectWaiter`对象列表(每个等待锁的线程都会被封装成`ObjectWaiter`对象)，`_owner`指向持有`ObjectMonitor`对象的线程，当多个线程同时访问一段同步代码时，首先会进入 `_EntryList `集合，当线程获取到对象的`monitor `后进入 `_Owner `区域并把`monitor`中的owner变量设置为当前线程同时monitor中的计数器count加1，若线程调用wait()方法，将释放当前持有的`monitor`，owner变量恢复为null，count自减1，同时该线程进入WaitSet集合中等待被唤醒。若当前线程执行完毕也将释放monitor(锁)并复位变量的值，以便其他线程进入获取monitor(锁)。如下图所示

  ![在这里插入图片描述](juc%E5%AD%A6%E4%B9%A0.assets/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L0RCQ18xMjE=,size_16,color_FFFFFF,t_70-167696981893717.png)



1. **Synchronized同步代码块的底层原理**

同步代码块的加锁、解锁是通过 Javac 编译器实现的，底层是借助`monitorenter`和`monitorerexit`，为了能够保证无论代码块正常执行结束 or 抛出异常结束，都能正确释放锁，Javac 编译器在编译的时候，会对`monitorerexit`进行特殊处理，举例说明：

```java
public class Hello {
    public void test() {
        synchronized (this) {
            System.out.println("test");
        }
    }
}

```

2. **通过 `javap -c` 查看其编译后的字节码:**

```java
public class Hello {
  public Hello();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return
  public void test();
    Code:
       0: aload_0
       1: dup
       2: astore_1
       3: monitorenter
       4: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
       7: ldc           #3                  // String test
       9: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      12: aload_1
      13: monitorexit
      14: goto          22
      17: astore_2
      18: aload_1
      19: monitorexit
      20: aload_2
      21: athrow
      22: return
    Exception table:
       from    to  target type
           4    14    17   any
          17    20    17   any
}

```

> 从字节码中可知同步语句块的实现使用的是`monitorenter`和`monitorexit`指令，其中`monitorenter`指令指向同步代码块的开始位置，`monitorexit`指令则指明同步代码块的结束位置，当执行`monitorenter`指令时，当前线程将试图获取mark word里面存储的monitor，当 monitor的进入计数器为 0，那线程可以成功取得monitor，并将计数器值设置为1，取锁成功。
>
> 如果当前线程已经拥有 monitor 的持有权，那它可以重入这个 monitor ，重入时计数器的值也会加 1。倘若其他线程已经拥有monitor的所有权，那当前线程将被阻塞，直到正在执行线程执行完毕，即`monitorexit`指令被执行，执行线程将释放 monitor并设置计数器值为0 ，其他线程将有机会持有 monitor 。
>
> 值得注意的是编译器将会确保无论方法通过何种方式完成，方法中调用过的每条 `monitorenter` 指令都有执行其对应 `monitorexit` 指令，而无论这个方法是正常结束还是异常结束。为了保证在方法异常完成时 `monitorenter` 和 `monitorexit` 指令依然可以正确配对执行，编译器会自动产生一个异常处理器，这个异常处理器声明可处理所有的异常，它的目的就是用来执行 `monitorexit` 指令。从上面的字节码中也可以看出有两个`monitorexit`指令，它就是异常结束时被执行的释放monitor 的指令

3. **同步方法底层原理**

同步方法的加锁、解锁是通过 Javac 编译器实现的，底层是借助`ACC_SYNCHRONIZED`访问标识符来实现的，代码如下所示：

```java
public class Hello {
    public synchronized void test() {
        System.out.println("test");
    }
}
```

> 方法级的同步是隐式，即无需通过字节码指令来控制的，它实现在方法调用和返回操作之中。JVM可以从方法常量池中的方法表结构(method_info Structure) 中的 ACC_SYNCHRONIZED 访问标志区分一个方法是否同步方法。当方法调用时，调用指令将会 检查方法的 ACC_SYNCHRONIZED访问标志是否被设置，如果设置了，执行线程将先持有monitor，然后再执行方法，最后在方法完成(无论是正常完成还是非正常完成)时释放monitor。在方法执行期间，执行线程持有了monitor，其他任何线程都无法再获得同一个monitor。如果一个同步方法执行期间抛出了异常，并且在方法内部无法处理此异常，那这个同步方法所持有的monitor将在异常抛到同步方法之外时自动释放。

字节码层面如何实现

```java
public class Hello {
  public Hello();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return
  public synchronized void test();
    Code:
       0: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: ldc           #3                  // String test
       5: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
       8: return
}
```

### 锁的其他优化

1. 适应性自旋（Adaptive Spinning）：从轻量级锁获取的流程中我们知道，当线程在获取轻量级锁的过程中执行CAS操作失败时，是要通过自旋来获取重量级锁的。问题在于，自旋是需要消耗CPU的，如果一直获取不到锁的话，那该线程就一直处在自旋状态，白白浪费CPU资源。解决这个问题最简单的办法就是指定自旋的次数，例如让其循环10次，如果还没获取到锁就进入阻塞状态。但是JDK采用了更聪明的方式——适应性自旋，简单来说就是线程如果自旋成功了，则下次自旋的次数会更多，如果自旋失败了，则自旋的次数就会减少。
2. 锁粗化（Lock Coarsening）：锁粗化的概念应该比较好理解，**在同一个锁的情况下**，就是将多次连接在一起的加锁、解锁操作合并为一次，将多个连续的锁扩展成一个范围更大的锁。）举个例子：

```java
public  void lockCoarsening() {
    int i=0;
    synchronized (this){
        i=i+1;
    }
    synchronized (this){
        i=i+2;
    }
}
```

3. 锁消除：锁消除即删除不必要的加锁操作的代码。比如下面的代码,因为加锁每次加的锁都是新创建的，不是唯一的，每个线程都拥有一把锁，因此起不到作用，JNI就将它删除不执行。

```java
public class LockRemove{
    static Object objectLock=new Object();
public void test(){
    Object obj= new Object();
    synchronized(obj){
       System.out.println(Thread.currentThread().getName() + " : " + obj.hashCode()+objectLock.hashCode());
    }
}
}
```

## 🤴AQS

[链接]:https://juejin.cn/post/6844903997438951437#heading-11

### 是什么，有什么用？

AQS全称`AbstractQueuedSynchronizer`，即抽象的队列同步器，是一种用来构建锁和同步器的框架。

**基于AQS构建同步器：**

- ReentrantLock
- Semaphore
- CountDownLatch
- ReentrantReadWriteLock
- SynchronusQueue
- FutureTask

**优势：**

- AQS 解决了在实现同步器时涉及的大量细节问题，例如自定义标准同步状态、FIFO 同步队列。
- 基于 AQS 来构建同步器可以带来很多好处。它不仅能够极大地减少实现工作，而且也不必处理在多个位置上发生的竞争问题

### AQS核心思想

> AQS的核心思想是使用一个volatile类型的int变量state来表示同步状态，线程通过CAS操作来争夺同步状态。AQS内部定义了一个Node类，每个进入等待队列的线程都会被封装成一个Node节点，并被插入到等待队列的末尾。在AQS中，每个Node节点都会维护自己的等待状态waitStatus，以及指向前驱和后继节点的引用prev和next。

如果被请求的共享资源空闲，则将当前请求资源的线程设置为有效的工作线程，并且将共享资源设置为锁定状态。如果被请求的共享资源被占用，那么就需要一套线程阻塞等待以及被唤醒时锁分配的机制，这个机制AQS是用CLH队列锁实现的，即将暂时获取不到锁的线程加入到队列中。

![img](juc学习.assets/16e653ec86038010tplv-t2oaga2asx-zoom-in-crop-mark4536000.webp)

**Sync queue：**同步队列，是一个双向列表。包括head节点和tail节点。head节点主要用作后续的调度。

![img](juc学习.assets/16e655b2ae715f93tplv-t2oaga2asx-zoom-in-crop-mark4536000.webp)

**Condition queue：** 非必须，单向列表。当程序中存在cindition的时候才会存在此列表。

![img](juc学习.assets/16e655fe5ff3bad2tplv-t2oaga2asx-zoom-in-crop-mark4536000.webp)

#### 设计思想

- AQS使用一个int成员变量来表示同步状态
- 使用Node实现FIFO队列，可以用于构建锁或者其他同步装置
- AQS资源共享方式：独占Exclusive（排它锁模式）和共享Share（共享锁模式）

> AQS它的所有子类中，要么实现并使用了它的独占功能的api，要么使用了共享锁的功能，而不会同时使用两套api，即便是最有名的子类ReentrantReadWriteLock也是通过两个内部类读锁和写锁分别实现了两套api来实现的

#### state状态

state状态使用volatile int类型的变量，表示当前同步状态。state的访问方式有三种:

- getState()
- setState()
- compareAndSetState()

#### Node节点waitStatus解析

每个Node节点包含了一个waitStatus字段，它表示当前节点的状态。waitStatus字段是一个整型值，其具体取值及其含义如下：

- 0：表示当前节点在等待队列中，还未被唤醒；
- 1：表示当前节点的前驱节点释放锁之后，需要唤醒当前节点；
- -1：表示当前节点已经被取消，不会参与同步队列中的竞争，也不会被唤醒；
- -2：表示当前节点需要被转移（transfer）到同步队列中；
- 其他值：在某些情况下，waitStatus还可能取其他值。

#### AQS中Node常量含义

> **CANCELLED**
>  waitStatus值为1时表示该线程节点已释放（超时、中断），已取消的节点不会再阻塞。

> **SIGNAL**
>  waitStatus为-1时表示该线程的后续线程需要阻塞，即只要前置节点释放锁，就会通知标识为 SIGNAL 状态的后续节点的线程

> **CONDITION**
>  waitStatus为-2时，表示该线程在condition队列中阻塞（Condition有使用）

> **PROPAGATE**
>  waitStatus为-3时，表示该线程以及后续线程进行无条件传播（CountDownLatch中有使用）共享模式下， PROPAGATE 状态的线程处于可运行状态

####  同步队列为什么称为FIFO呢？

因为只有前驱节点是head节点的节点才能被首先唤醒去进行同步状态的获取。当该节点获取到同步状态时，它会清除自己的值，将自己作为head节点，以便唤醒下一个节点。

#### Condition队列（了解）

​	除了同步队列之外，AQS中还存在Condition队列，这是一个单向队列。调用ConditionObject.await()方法，能够将当前线程封装成Node加入到Condition队列的末尾，然后将获取的同步状态释放（即修改同步状态的值，唤醒在同步队列中的线程）。

> Condition队列也是FIFO。调用ConditionObject.signal()方法，能够唤醒firstWaiter节点，将其添加到同步队列末尾。

### 🧧实现细节

> 线程首先尝试获取锁，如果失败就将当前线程及等待状态等信息包装成一个node节点加入到FIFO队列中。 接着会不断的循环尝试获取锁，条件是当前节点为head的直接后继才会尝试。如果失败就会阻塞自己直到自己被唤醒。而当持有锁的线程释放锁的时候，会唤醒队列中的后继线程。

> AQS的同步实现主要分为三个部分：获取同步状态、释放同步状态和队列操作。在获取同步状态时，AQS会先通过tryAcquire()方法尝试获取同步状态，如果获取失败，则会将当前线程封装成Node节点并加入到等待队列中，然后进入自旋状态，不断尝试获取同步状态。在释放同步状态时，AQS会先通过tryRelease()方法释放同步状态，然后唤醒等待队列中的后继节点，使其尝试获取同步状态。队列操作主要包括入队和出队两个操作，这些操作都是通过CAS原子操作实现的，确保了线程的安全性。

#### 独占模式下的AQS

**所谓独占模式**，即只允许一个线程获取同步状态，当这个线程还没有释放同步状态时，其他线程是获取不了的，只能加入到同步队列，进行等待。

> 很明显，我们可以将state的初始值设为0，表示空闲。当一个线程获取到同步状态时，利用CAS操作让state加1，表示非空闲，那么其他线程就只能等待了。释放同步状态时，不需要CAS操作，因为独占模式下只有一个线程能获取到同步状态。ReentrantLock、CyclicBarrier正是基于此设计的。

##### 独占模式获取资源-acquire方法

acquire以独占exclusive方式获取资源。如果获取到资源，线程直接返回，否则进入等待队列，直到获取到资源为止

```java
 public final void acquire(int arg) {
        if (!tryAcquire(arg) &&
            acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
            selfInterrupt();
    }
```

![img](https://gitee.com/xiongshengyu/juclearning/raw/master/img/16e65b5c00dccb3dtplv-t2oaga2asx-zoom-in-crop-mark4536000.webp)

* `tryAcquire`方法尝试获取锁，获取到就返回true
* 如果没成功，调用`addWaiter`方法，将新节点添加到等待队列尾部，并标记为独占模式。如果队列还没有，就先创建队列，有一个虚拟节点
* `acquireQueued()`使线程在等待队列中休息，有机会时（轮到自己，会被`unpark()`）会去尝试获取资源。获取到资源后才返回。如果在整个等待过程中被中断过，则返回true，否则返回false。

- 如果线程在等待过程中被中断过，它是不响应的。只是获取资源后才再进行自我中断`selfInterrupt()`，将中断补上。

##### 获取资源tryAcquire方法

**AQS定义的尝试获取锁方法，由具体的子类实现**

```java
 protected boolean tryAcquire(int arg) {
        throw new UnsupportedOperationException();
    }
```

**实现AQS的`tryAcquire`**

```java
 protected final boolean tryAcquire(int acquires) {
            return nonfairTryAcquire(acquires);
        }
```



```java
final boolean nonfairTryAcquire(int acquires) {
            final Thread current = Thread.currentThread();
            int c = getState();
            if (c == 0) {
                if (compareAndSetState(0, acquires)) {
                    setExclusiveOwnerThread(current);
                    return true;
                }
            }
            else if (current == getExclusiveOwnerThread()) {
                int nextc = c + acquires;
                if (nextc < 0) // overflow
                    throw new Error("Maximum lock count exceeded");
                setState(nextc);
                return true;
            }
            return false;
        }
```

> **分析：**
>
> 获取锁的状态，0代表没有线程占有，1代表有线程持有，如果没有线程占有，利用CAS操作将锁状态变为1，设置当前持有线程为本线程。
>
> 如果有线程持有，判断是否为当前线程。如果是当前线程持有，代表可重入锁，增加重入次数，更新锁的状态`state`

> **理解：**
>
> * `tryAcquire`方法有AQS定义行为，具体的行为有子类去实现。
> * 具体过程如下：
>   1. 根据state的值判断是否有线程占有锁，如果没有，尝试去获取，利用CAS操作尝试修改state值为1，获取成功就返回
>   2. 如果有线程占有锁（state值为1）判断是否是当前线程占有，如果是当前线程占有，代表可重入锁。增加可重入次数，更新锁的状态

##### 进入等待队列-addWaiter方法

根据不同模式(`Node.EXCLUSIVE`互斥模式、`Node.SHARED`共享模式)创建结点并以CAS的方式将当前线程节点加入到不为空的等待队列的末尾(通过`compareAndSetTail()`方法)。如果队列为空，通过`enq(node)`方法初始化一个等待队列，并返回当前节点。

```java
/**
* 参数
* @param mode Node.EXCLUSIVE for exclusive, Node.SHARED for shared
* 返回值
* @return the new node
*/
private Node addWaiter(Node mode) {
    //将当前线程以指定的模式创建节点node
    Node node = new Node(Thread.currentThread(), mode);
    // Try the fast path of enq; backup to full enq on failure
    // 获取当前同队列的尾节点
    Node pred = tail;
    //队列不为空，将新的node加入等待队列中
    if (pred != null) {
        node.prev = pred;
         //CAS方式将当前节点尾插入队列中
        if (compareAndSetTail(pred, node)) {
            pred.next = node;
            return node;
        }
    }
    //当队列为empty或者CAS失败时会调用enq方法处理
    enq(node);
    return node;
}
```

其中，队列为empty，使用`enq(node)`处理，将当前节点插入等待队列，如果队列为空，则初始化当前队列。所有操作都是CAS自旋的方式进行，直到成功加入队尾为止。

```java
 private Node enq(final Node node) {
        //不断自旋
        for (;;) {
            Node t = tail;
            //当前队列为empty
            if (t == null) { // Must initialize
             //完成队列初始化操作，虚拟头结点中不放数据，只是作为起始标记，lazy-load，在第一次用的时候new
                if (compareAndSetHead(new Node()))
                    tail = head;
            } else {
                node.prev = t;
                //不断将当前节点使用CAS尾插入队列中直到成功为止
                if (compareAndSetTail(t, node)) {
                    t.next = node;
                    return t;
                }
            }
        }
    }
```

> **理解分析：**
>
> * `addWaiter`方法主要是在线程尝试获取锁失败之后，将线程放入等待队列的过程
> * 过程如下：
>   1. 首先根据指定的模式，创建Node节点，判断等待队列是否为空，如果不为空就利用CAS操作将新节点插入等待队列尾部。
>   2. 如果等待队列为空，则调用`enq`方法初始化队列，并将新节点插入尾部，队列有一个虚拟头节点
>      1. 自旋操作完成队列初始化以及插入新节点。
>      2. 判断一下当前队列是否为空，为空就利用CAS操作创建一个虚拟头节点，并将`head`指针和`tail`指针指向虚拟头结点
>      3. 队列初始化完成，利用CAS操作将新节点插入队列尾部，返回新节点。

##### 等待队列中获取锁-acquireQueued方法

`acquireQueued`用于已在队列中的线程以独占且不间断模式获取state状态，直到获取锁后返回。主要流程：

- 结点node进入队列尾部后，检查状态；
- 调用park()进入waiting状态，等待unpark()或interrupt()唤醒；
- 被唤醒后，是否获取到锁。如果获取到，head指向当前结点，并返回从入队到获取锁的整个过程中是否被中断过；如果没获取到，继续流程1

```java
final boolean acquireQueued(final Node node, int arg) {
  //是否已获取锁的标志，默认为true 即为尚未
  boolean failed = true;
  try {
      //等待中是否被中断过的标记
      boolean interrupted = false;
      for (;;) {
          //获取前节点
          final Node p = node.predecessor();
          //如果当前节点已经成为头结点，尝试获取锁（tryAcquire）成功，然后返回
          if (p == head && tryAcquire(arg)) {
              setHead(node);
              p.next = null; // help GC
              failed = false;
              return interrupted;
          }
          //shouldParkAfterFailedAcquire根据对当前节点的前一个节点的状态进行判断，对当前节点做出不同的操作
          //parkAndCheckInterrupt让线程进入等待状态，并检查当前线程是否被可以被中断
          if (shouldParkAfterFailedAcquire(p, node) &&
              parkAndCheckInterrupt())
              interrupted = true;
      }
  } finally {
      //将当前节点设置为取消状态；取消状态设置为1
      if (failed)
          cancelAcquire(node);
  }
}
```

> **理解：**
>
> * `acquireQueued`方法用于将线程放入等待队列后，能够让等待队列中的线程获取锁，获取到锁之后返回。
> * 具体过程如下：
>   1. 自旋尝试获取锁
>   2. 首先获取当前节点的前一个节点，判断前节点是否为头结点，如果是，尝试获取锁，获取到之后将当前节点设置为头结点，断开与前节点之间的关系等待垃圾回收器回收。返回是否中断。
>   3. 如果前节点不是头结点，或者尝试获取锁失败，就根据前节点的状态（waitStates），对当前节点做出不同的操作。一般状况下是，前节点状态位0，就利用CAS操作设置前节点状态位为SIGNAL，意思是该节点的后节点（当前节点）需要被阻塞。如果前节点的状态位是SIGNAL（-1）就返回true，继续执行`parkAndCheckInterrupt`
>   4. 调用`parkAndCheckInterrupt`方法其中利用`LockSupport.park(this)`将当前线程阻塞，直到被唤醒（可能是由其他线程释放锁或当前线程被中断）
>   5. 被唤醒之后继续尝试获取锁，获取到就返回，没获取到就继续该流程

##### 释放锁-release方法

release方法是独占exclusive模式下线程释放共享资源的锁。它会调用tryRelease()释放同步资源，如果全部释放了同步状态为空闲（即state=0）,当同步状态为空闲时，它会唤醒等待队列里的其他线程来获取资源。这也正是unlock()的语义，当然不仅仅只限于unlock().

```java
public final boolean release(int arg) {
    if (tryRelease(arg)) {
        Node h = head;
        if (h != null && h.waitStatus != 0)
            unparkSuccessor(h);
        return true;
    }
    return false;
}
```

##### 释放锁-tryRelease方法

```java
 protected boolean tryRelease(int arg) {
        throw new UnsupportedOperationException();
    }
```

**ReentrantReadWriteLock的实现：**

```java
protected final boolean tryRelease(int releases) {
    int c = getState() - releases;
    if (Thread.currentThread() != getExclusiveOwnerThread())
        throw new IllegalMonitorStateException();
    boolean free = false;
    if (c == 0) {
        free = true;
        setExclusiveOwnerThread(null);
    }
    setState(c);
    return free;
}
```

> **分析：**
>
> * `tryRelease`该方法是锁的释放方法，该方法会修改同步状态，释放锁，并将锁的拥有者线程置为null。如果锁被完全释放，则返回`true`，否则返回`false`。只能由独占锁的持有者线程调用
> * 具体过程：
>   1. 将同步状态减掉指定的次数，判断当前线程是否拥有独占锁，如果没有，抛出`IllegalMonitorStateException`异常
>   2. 如果同步状态为0，代表锁已经完全释放。free变量设置为true,代表已经释放了锁，同时将当前锁的拥有者设置为null，表示没有线程持有锁。
>   3. 设置新的同步状态，返回free变量值，表示是否释放锁

##### 释放锁-unparkSuccessor方法

`unparkSuccessor`用unpark()唤醒等待队列中最前驱的那个未放弃线程，此线程并不一定是当前节点的next节点，而是下一个可以用来唤醒的线程，如果这个节点存在，调用unpark()方法唤醒。`unparkSuccessor` 方法的作用是唤醒当前节点的后继节点，使得后继节点能够尝试获取锁。

```java
 private void unparkSuccessor(Node node) {
    // 获取当前节点的等待状态
    int ws = node.waitStatus;
    //置零当前线程所在的结点状态，允许失败，如果等待状态为负数，即可能需要信号，则尝试清除等待状态
     //即将等待状态改为0，如果尝试失败或者等待状态在等待期间被其他线程改变了，那么忽略即可
    if (ws < 0)
        compareAndSetWaitStatus(node, ws, 0);
    // 获取当前节点的后继节点
    Node s = node.next;
      // 如果后继节点为空或者等待状态大于0，说明后继节点已经取消或者被阻塞了
    // 需要从队列的尾节点开始向前遍历，寻找一个等待状态不大于0的节点作为后继节点
    if (s == null || s.waitStatus > 0) {
        s = null;
        // 从后向前找
        for (Node t = tail; t != null && t != node; t = t.prev)
            //从这里可以看出，<=0的结点，都是还有效的结点
            if (t.waitStatus <= 0)
                s = t;
    }
     // 如果找到了后继节点，则唤醒它对应的线程，使其可以继续尝试获取锁
    if (s != null)
        LockSupport.unpark(s.thread);
}
```

#### 共享模式的AQS

`acquireShared`在共享模式下线程获取共享资源的顶层入口。它会获取指定量的资源，获取成功则直接返回，获取失败则进入等待队列，直到获取到资源为止，整个过程忽略中断。

```java
public final void acquireShared(int arg) {
        if (tryAcquireShared(arg) < 0)
            doAcquireShared(arg);
    }
```

流程：

- 先通过tryAcquireShared()尝试获取资源，成功则直接返回；
- 失败则通过doAcquireShared()中的park()进入等待队列，直到被unpark()/interrupt()并成功获取到资源才返回(整个等待过程也是忽略中断响应)。

##### 共享模式获取资源-tryAcquireShared方法

`tryAcquireShared()`跟独占模式获取资源方法一样实现都是由自定义同步器去实现。但AQS规范中已定义好`tryAcquireShared()`的返回值：

- 负值代表获取失败；
- 0代表获取成功，但没有剩余资源；
- 正数表示获取成功，还有剩余资源，其他线程还可以去获取。

```java
 protected int tryAcquireShared(int arg) {
        throw new UnsupportedOperationException();
    }
```

##### 共享模式获取资源-doAcquireShared方法

`doAcquireShared()`用于将当前线程加入等待队列尾部休息，直到其他线程释放资源唤醒自己，自己成功拿到相应量的资源后才返回。

```java
private void doAcquireShared(int arg) {
    //加入队列尾部
    final Node node = addWaiter(Node.SHARED);
    //是否成功标志
    boolean failed = true;
    try {
        //等待过程中是否被中断过的标志
        boolean interrupted = false;
        for (;;) {
            final Node p = node.predecessor();//获取前驱节点
            if (p == head) {//如果到head的下一个，因为head是拿到资源的线程，此时node被唤醒，很可能是head用完资源来唤醒自己的
                int r = tryAcquireShared(arg);//尝试获取资源
                if (r >= 0) {//成功
                    setHeadAndPropagate(node, r);//将head指向自己，还有剩余资源可以再唤醒之后的线程
                    p.next = null; // help GC
                    if (interrupted)//如果等待过程中被打断过，此时将中断补上。
                        selfInterrupt();
                    failed = false;
                    return;
                }
            }
            
            //判断状态，队列寻找一个适合位置，进入waiting状态，等着被unpark()或interrupt()
            if (shouldParkAfterFailedAcquire(p, node) &&
                parkAndCheckInterrupt())
                interrupted = true;
        }
    } finally {
        if (failed)
            cancelAcquire(node);
    }   
}
```

> **理解分析：**
>
> * 创建一个类型为Node.SHARED的等待节点，然后加入等待队列中（使用addWaiter方法）。
> * 自旋获取共享锁，获取当前节点的前一个节点，如果前节点是头结点，就尝试获取共享锁（使用tryAcquireShared方法）
> * 如果共享锁获取成功，就将当前节点设置为头结点，并传播唤醒信号（使用setHeadAndPropagate方法）并将p节点的next指针置为null，最后将failed标记设置为false并返回。
> * 如果共享锁获取失败，则会根据shouldParkAfterFailedAcquire方法的返回值来决定是否要将当前线程阻塞（使用parkAndCheckInterrupt方法）。如果当前线程被中断，则会将interrupted标记设置为true。
> * 当整个自选操作结束后，如果还没有成功获取共享锁，则会调用cancelAcquire方法取消对共享锁的获取，将该节点从等待队列中移除。

##### 共享模式释放资源-releaseShared方法

`releaseShared()`用于共享模式下线程释放共享资源，释放指定量的资源，如果成功释放且允许唤醒等待线程，它会唤醒等待队列里的其他线程来获取资源。

```java
public final boolean releaseShared(int arg) {
    //尝试释放资源
    if (tryReleaseShared(arg)) {
        //唤醒后继结点
        doReleaseShared();
        return true;
    }
    return false;
}
```

> 独占模式下的tryRelease()在完全释放掉资源（state=0）后，才会返回true去唤醒其他线程，这主要是基于独占下可重入的考量；而共享模式下的releaseShared()则没有这种要求，共享模式实质就是控制一定量的线程并发执行，那么拥有资源的线程在释放掉部分资源时就可以唤醒后继等待结点。

##### 共享模式释放资源-doReleaseShared方法

```java
private void doReleaseShared() {
    for (;;) {
        Node h = head;
        if (h != null && h != tail) {
            int ws = h.waitStatus;
            if (ws == Node.SIGNAL) {
                if (!compareAndSetWaitStatus(h, Node.SIGNAL, 0))
                    continue;
                    //唤醒后继
                unparkSuccessor(h);
            }
            else if (ws == 0 &&
                     !compareAndSetWaitStatus(h, 0, Node.PROPAGATE))
                continue;
        }
        // head发生变化
        if (h == head)
            break;
    }
}
```



## ReentrantReadWriteLock（读写锁）

**概念：**

`ReentrantReadWriteLock`是Java并发包中提供的一个实现读写锁的类，可以提高对共享资源的访问效率。它与普通的重入锁不同之处在于，`ReentrantReadWriteLock`有两个锁：**读锁**和**写锁**。多个线程可以同时持有读锁，但只有一个线程可以持有写锁。如果有线程持有写锁，则所有其他线程都不能持有读锁或写锁，直到写锁被释放。读锁和写锁之间的互斥关系可以防止写操作与其他读操作或写操作冲突，保证了数据的一致性。

> **特点：**
>
> 1. **公平性选择：**
>    * 支持非公平性（默认）和公平的锁获取方式，吞吐量还是非公平优于公平；
> 2. **重入性**：
>    * 支持重入，读锁获取后能再次获取，写锁获取之后能够再次获取写锁，同时也能够获取读锁；（读锁获取后不能再获取写锁）
> 3. **锁降级**：遵循获取写锁，获取读锁再释放写锁的次序，写锁能够降级成为读锁

### 写锁

#### 写锁的获取

在同一时刻写锁是不能被多个线程所获取，很显然写锁是独占式锁，而实现写锁的同步语义是通过重写AQS中的tryAcquire方法实现的。源码为:

```java
protected final boolean tryAcquire(int acquires) {
    /*
     * Walkthrough:
     * 1. If read count nonzero or write count nonzero
     *    and owner is a different thread, fail.
     * 2. If count would saturate, fail. (This can only
     *    happen if count is already nonzero.)
     * 3. Otherwise, this thread is eligible for lock if
     *    it is either a reentrant acquire or
     *    queue policy allows it. If so, update state
     *    and set owner.
     */
    Thread current = Thread.currentThread();
	// 1. 获取写锁当前的同步状态
    int c = getState();
	// 2. 获取写锁获取的次数
    int w = exclusiveCount(c);
    if (c != 0) {
        // (Note: if c != 0 and w == 0 then shared count != 0)
		// 3.1 当读锁已被读线程获取或者当前线程不是已经获取写锁的线程的话
		// 当前线程获取写锁失败
        if (w == 0 || current != getExclusiveOwnerThread())
            return false;
        if (w + exclusiveCount(acquires) > MAX_COUNT)
            throw new Error("Maximum lock count exceeded");
        // Reentrant acquire
		// 3.2 当前线程获取写锁，支持可重复加锁
        setState(c + acquires);
        return true;
    }
	// 3.3 写锁未被任何线程获取，当前线程可获取写锁
    if (writerShouldBlock() ||
        !compareAndSetState(c, c + acquires))
        return false;
    setExclusiveOwnerThread(current);
    return true;
}
```

> **分析：**
>
> 变量c代表当前锁的状态，其值包括两部分：
>
> - 高位表示当前已经获取读锁的线程数量（最多2^16-1个线程）。
> - 低位表示当前正在持有写锁的线程数量（最多1个线程）。
>
> 1. 如果读锁或写锁已经被持有，并且当前线程不是持有锁的线程，则获取锁失败。
> 2. 如果锁数量已经达到上限 MAX_COUNT，则获取锁失败。
> 3. 如果当前线程是重入获取锁，或者队列策略允许当前线程获取锁，则更新锁的状态并将当前线程设置为锁的持有者，获取锁成功。
> 4. 如果写锁被阻塞或者更新状态失败，则获取锁失败。

#### 写锁的释放

写锁释放通过重写AQS的tryRelease方法，源码为：

```java
protected final boolean tryRelease(int releases) {
    if (!isHeldExclusively())
        throw new IllegalMonitorStateException();
	//1. 同步状态减去写状态
    int nextc = getState() - releases;
	//2. 当前写状态是否为0，为0则释放写锁
    boolean free = exclusiveCount(nextc) == 0;
    if (free)
        setExclusiveOwnerThread(null);
	//3. 不为0则更新同步状态
    setState(nextc);
    return free;
}
```

这里需要注意的是，减少写状态`int nextc = getState() - releases;`只需要用**当前同步状态直接减去写状态的原因正是我们刚才所说的写状态是由同步状态的低16位表示的**

### 读锁详解

#### 读锁的获取

读锁不是独占式锁，即同一时刻该锁可以被多个读线程获取也就是一种共享式锁。实现共享式同步组件的同步语义需要通过重写AQS的tryAcquireShared方法和tryReleaseShared方法。读锁的获取实现方法为：

```java
protected final int tryAcquireShared(int unused) {
    /*
     * Walkthrough:
     * 1. If write lock held by another thread, fail.
     * 2. Otherwise, this thread is eligible for
     *    lock wrt state, so ask if it should block
     *    because of queue policy. If not, try
     *    to grant by CASing state and updating count.
     *    Note that step does not check for reentrant
     *    acquires, which is postponed to full version
     *    to avoid having to check hold count in
     *    the more typical non-reentrant case.
     * 3. If step 2 fails either because thread
     *    apparently not eligible or CAS fails or count
     *    saturated, chain to version with full retry loop.
     */
    Thread current = Thread.currentThread();
    int c = getState();
	//1. 如果写锁已经被获取并且获取写锁的线程不是当前线程的话，当前
	// 线程获取读锁失败返回-1
    if (exclusiveCount(c) != 0 &&
        getExclusiveOwnerThread() != current)
        return -1;
    int r = sharedCount(c);
    if (!readerShouldBlock() &&
        r < MAX_COUNT &&
		//2. 当前线程获取读锁
        compareAndSetState(c, c + SHARED_UNIT)) {
		//3. 下面的代码主要是新增的一些功能，比如getReadHoldCount()方法
		//返回当前获取读锁的次数
        if (r == 0) {
            firstReader = current;
            firstReaderHoldCount = 1;
        } else if (firstReader == current) {
            firstReaderHoldCount++;
        } else {
            HoldCounter rh = cachedHoldCounter;
            if (rh == null || rh.tid != getThreadId(current))
                cachedHoldCounter = rh = readHolds.get();
            else if (rh.count == 0)
                readHolds.set(rh);
            rh.count++;
        }
        return 1;
    }
	//4. 处理在第二步中CAS操作失败的自旋已经实现重入性
    return fullTryAcquireShared(current);
}
```

> **分析：**
>
> 1. 获取锁的状态，`exclusiveCount(c)`该方法返回写锁的持有数，如果有线程持有写锁，并且持有写锁的线程不是该线程，返回-1，表示获取失败
> 2. 获取共享锁（读锁）的数量，判断当前是否应该阻塞，以及当前是否可以继续获取共享锁，如果当前不需要阻塞，共享锁数量还未达到最大值，并且通过 CAS 修改成功了当前状态，那么就可以尝试获取共享锁。
> 3. 如果当前是第一个获取共享锁的线程，将当前线程作为第一个读线程，并记录当前线程的持有共享锁的数量
> 4. 如果当前线程是第一个读线程，将该线程的持有数量+1
> 5. 如果当前线程既不是第一个获取读锁的线程，也不是第一个读线程，就从缓存中获取线程持有读锁的数量，如果没有就创建一个，将当前线程持有读锁数量+1
> 6. 返回 1代表获取读锁成功
> 7. 获取共享锁失败，调用 fullTryAcquireShared() 方法继续尝试获取共享锁

#### 读锁的释放

```java
protected final boolean tryReleaseShared(int unused) {
    Thread current = Thread.currentThread();
	// 前面还是为了实现getReadHoldCount等新功能
    if (firstReader == current) {
        // assert firstReaderHoldCount > 0;
        if (firstReaderHoldCount == 1)
            firstReader = null;
        else
            firstReaderHoldCount--;
    } else {
        HoldCounter rh = cachedHoldCounter;
        if (rh == null || rh.tid != getThreadId(current))
            rh = readHolds.get();
        int count = rh.count;
        if (count <= 1) {
            readHolds.remove();
            if (count <= 0)
                throw unmatchedUnlockException();
        }
        --rh.count;
    }
    for (;;) {
        int c = getState();
		// 读锁释放 将同步状态减去读状态即可
        int nextc = c - SHARED_UNIT;
        if (compareAndSetState(c, nextc))
            // Releasing the read lock has no effect on readers,
            // but it may allow waiting writers to proceed if
            // both read and write locks are now free.
            return nextc == 0;
    }
}
```

> 

### 锁降级

读写锁支持锁降级，**遵循按照获取写锁，获取读锁再释放写锁的次序，写锁能够降级成为读锁**，不支持锁升级，关于锁降级下面的示例代码摘自ReentrantWriteReadLock源码中：

```java
void processCachedData() {
        rwl.readLock().lock();
        if (!cacheValid) {
            // Must release read lock before acquiring write lock
            rwl.readLock().unlock();
            rwl.writeLock().lock();
            try {
                // Recheck state because another thread might have
                // acquired write lock and changed state before we did.
                if (!cacheValid) {
                    data = ...
            cacheValid = true;
          }
          // Downgrade by acquiring read lock before releasing write lock
          rwl.readLock().lock();
        } finally {
          rwl.writeLock().unlock(); // Unlock write, still hold read
        }
      }
 
      try {
        use(data);
      } finally {
        rwl.readLock().unlock();
      }
    }
}
```

> **理解分析：**
>
> * 锁降级是指在**持有写锁时，获取读锁，然后释放写锁，用读锁代替写锁**
> * 这样做的好处在于，可以在保证数据一致性的前提下，允许多个线程同时访问共享数据，从而提高程序的并发性能。

## StampedLock（邮戳锁/版本锁）

StampedLock是Java 8引入的一种锁机制，它支持乐观读取、悲观读取和写入操作。在读操作比写操作更多的情况下，它比ReentrantReadWriteLock提供更高的并发性能。

**解决的问题**
ReentrantReadWriteLock在读线程非常多，写线程很少的情况下，很容易导致写线程“**饥饿**”，虽然使用“公平”策略可以一定程度上缓解这个问题，但是“公平”策略是以牺牲系统吞吐量为代价的。

### StampedLock的使用方法

StampedLock提供了三种方法来获取标记：readLock()、writeLock()和tryOptimisticRead()。

- readLock()方法获取一个悲观读锁，会阻止其他线程获取写锁，但不会阻止其他线程获取读锁。当有一个线程持有写锁时，其他线程想要获取读锁时会被阻塞，直到写锁被释放。
- writeLock()方法获取一个写锁，会阻止其他线程获取读锁和写锁。当一个线程持有读锁或写锁时，其他线程想要获取写锁时会被阻塞，直到所有读锁和写锁都被释放。
- tryOptimisticRead()方法获取一个乐观读锁。这个方法不会阻塞其他线程，它只是返回一个非0的标记。

StampedLock还提供了一些其他方法，例如tryConvertToWriteLock()和tryConvertToReadLock()，这些方法可以将当前线程持有的锁转换为另一种类型的锁，以避免死锁。同时，StampedLock还支持可重入
